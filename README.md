# **Final Praktikum**

## **Anggota Kelompok**

| NRP        | Nama                      |
| ---        | ---                       |
| 5025211235 | Ketut Arda Putra Mahotama |
| 5025211003 | Clarissa Luna Maheswari   |
| 5025211127 | Nadif Mustafa             |

<br>

## **Daftar Isi**

- [**Final Praktikum**](#final-praktikum)
  - [**Anggota Kelompok**](#anggota-kelompok)
  - [**Daftar Isi**](#daftar-isi)
  - [**Client**](#client)
    - [**Inisialisasi Socket dan Koneksi ke Server**](#inisialisasi-socket-dan-koneksi-ke-server)
    - [**Autentikasi dan Input**](#autentikasi-dan-input)
    - [**Cara Menggunakan**](#cara-menggunakan)
  - [**Database**](#database)
    - [**Prasyarat**](#prasyarat)
    - [**Konfigurasi**](#konfigurasi)
    - [**Utilitas String**](#utilitas-string)
    - [**Utilitas Lainnya**](#utilitas-lainnya)
    - [**Penanganan Perintah**](#penanganan-perintah)
    - [**Fungsi Utama**](#fungsi-utama)
    - [**Detail**](#detail)
      - [**String Utilities**](#string-utilities)
      - [**More Utilities**](#more-utilities)
      - [**Handle Commands**](#handle-commands)
    - [**Cara Menggunakan**](#cara-menggunakan-1)
  - [**Containerization**](#containerization)
    - [**Dockerfile**](#dockerfile)
    - [**docker-compose.yml**](#docker-compose.yml)
    - [**Cara Menggunakan**](#cara-menggunakan-2)
  - [**Dump**](#dump)
    - [**Inisialisasi Socket dan Koneksi ke Server**](#inisialisasi-socket-dan-koneksi-ke-server-1)
    - [**Autentikasi dan Input**](#autentikasi-dan-input-1)
    - [**Cara Menggunakan**](#cara-menggunakan-3)

<br>

## **Client**

Program_client.c digunakan untuk berinteraksi dengan server menggunakan protokol TCP/IP. Program ini memungkinkan pengguna untuk mengirim perintah ke server dan menerima respons dari servefr.

<br>

### **Inisialisasi Socket dan Koneksi ke Server**

<br>

```c
// Initiate Client
struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;

if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
{
    printf("\n Socket creation error \n");
    return -1;
}

memset(&serv_addr, '0', sizeof(serv_addr));

serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(PORT);

// IP Docker : 172.28.169.226
// IP Local : 127.0.0.1
if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
{
    printf("\nInvalid address/ Address not supported \n");
    return -1;
}

if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
{
    printf("\nConnection Failed \n");
    return -1;
}
```

Pada langkah ini, kita membuat socket menggunakan fungsi `socket()` dan menginisialisasi struktur `serv_addr` yang berisi alamat IP dan port server yang akan dihubungi. Selanjutnya, kita melakukan koneksi ke server menggunakan fungsi `connect()`.

<br>

### **Autentikasi dan Input**

<br>

```c
// Important Variables
char user[STR_BUFFER];
char database[OUT_BUFFER], response[OUT_BUFFER];
char buffer[CMD_BUFFER], input[CMD_BUFFER];

// User
if (getuid() == 0)
    strcpy(user, "root");
else
    strcpy(user, "user");

// Console Input
if (isatty(fileno(stdin)))
{
    int login = 1;

    while (1)
    {
        init(user, database, response, buffer, input, &valread, sock);

        if(login){
            strcpy(input, "LOGIN");
            for (int i = 1; i < argc; ++i){
                strcat(input, " ");
                strcat(input, argv[i]);
            }
        }
        else {
            printf("%s=#", strcmp(database, "databases") == 0 ? "E07sql" : database);
            fgets(input, CMD_BUFFER, stdin);
            input[strcspn(input, "\n")] = 0;
        }
        strcat(buffer, input);

        send(sock, buffer, CMD_BUFFER, 0);

        if (strcasecmp(input, "EXIT") == 0)
        {
            exit(EXIT_SUCCESS);
        }

        printf("Waiting for response...\n");
        valread = read(sock, response, OUT_BUFFER);
        printf("%s\n", response);

        if(login){
            if(strcmp(response, "Login Success!") == 0){
                if (strcmp(user, "user") == 0)
                    strcpy(user, argv[2]);
                login = 0;
            }
            else {
                exit(EXIT_FAILURE);
            }
        }
    }
}
// File Input
else {
    // Login
    init(user, database, response, buffer, input, &valread, sock);

    // checking
    strcpy(input, "READ");
    for (int i = 1; i < argc; ++i){
        strcat(input, " ");
        strcat(input, argv[i]);
    }
    strcat(buffer, input);
    send(sock, buffer, CMD_BUFFER, 0);

    printf("Waiting for response...\n");
    valread = read(sock, response, OUT_BUFFER);
    printf("%s\n", response);

    if(strcmp(response, "All checking success!") == 0){
        if (strcmp(user, "user") == 0)
            strcpy(user, argv[2]);
    }
    else {
        exit(EXIT_FAILURE);
    }

    // Read Input
    while (fgets(input, CMD_BUFFER, stdin) != NULL)
    {
        if(strcmp(input, "") != 0){
            // Preparation
            init(user, database, response, buffer, input, &valread, sock);

            // Input
            input[strcspn(input, "\n")] = 0;
            strcat(buffer, input);
            send(sock, buffer, CMD_BUFFER, 0);

            // Response
            printf("Waiting for response...\n");
            valread = read(sock, response, OUT_BUFFER);
            printf("%s\n", response);
        }
    }
    init(user, database, response, buffer, input, &valread, sock);
    strcat(buffer, "EXIT");
    send(sock, buffer, CMD_BUFFER, 0);
}
```

Pada langkah ini, kita melakukan autentikasi dengan menentukan username yang akan digunakan. Jika program dijalankan dengan hak akses root, maka username akan diatur sebagai "root". Jika bukan root, maka username akan diatur sebagai "user". Kemudian untuk proses autentikasi akan di-*handle* di server.

Selanjutnya, jika proses autentikasi berhasil, maka user bisa melanjutkan memasukkan perintah-perintah. Jika tidak, maka program client akan berhenti. Respon akan diterima dari server dan akan ditampilkan pada layar client.

Selain itu, proses menerima input perintah juga dibedakan berdasarkan apakah program client menerima input dari layar console atau dari suatu file. Jika dari console, maka program akan terus berjalan hingga client memasukkan `EXIT`. Jika dari file, maka program akan terus berjalan hingga `End-Of-File`.

<br>

### **Cara Menggunakan**

1. Pastikan server sudah berjalan dan dapat diakses melalui alamat IP dan port yang sesuai.
2. Compile program client menggunakan kompilator C seperti GCC.
3. Jalankan program client yang telah dikompilasi.
4. Masukkan username, password, dan perintah yang sesuai untuk dikirim ke server.
5. Program akan menampilkan respons dari server.

<br>

## **Database**

Program ini mengimplementasikan server database sederhana yang mendengarkan koneksi masuk dan memproses perintah-perintah mirip SQL dari klien. Server ini memungkinkan pengguna untuk membuat database, membuat tabel, memasukkan data, menghapus data, memperbarui data, dan melakukan kueri select pada data yang disimpan.

<br>

### **Prasyarat**

Sebelum menjalankan program, dependensi berikut ini harus terpasang:

- `time.h`
- `sys/socket.h`
- `stdlib.h`
- `netinet/in.h`
- `semaphore.h`
- `string.h`
- `unistd.h`
- `fcntl.h`
- `ctype.h`

<br>

### **Konfigurasi**

Ada beberapa variabel dan konstanta penting yang didefinisikan di awal program:

- `PORT`: Port yang digunakan oleh server untuk menerima koneksi dari klien. Defaultnya adalah 8080.

- `CMD_BUFFER`: Ukuran buffer untuk menyimpan perintah dari klien.

- `STR_BUFFER`: Ukuran buffer untuk manipulasi string.

- `OUT_BUFFER`: Ukuran buffer untuk menyimpan output yang akan dikirimkan kembali ke klien.

- `CMD_IDX`: Indeks dari perintah dalam array token.

- `SUB_CMD_IDX`: Indeks dari sub-perintah dalam array token.

- `MAX_TOKENS`: Jumlah maksimum token yang dapat diproses dalam satu perintah.

- `databases_path`: Path direktori untuk menyimpan database.

- `log_path`: Path file untuk menyimpan log aktivitas.

- `users_path`: Path file untuk menyimpan informasi pengguna.

- `sem`: Semaphore untuk mengatur akses ke database.

### **Utilitas String**

Program ini juga menyediakan beberapa fungsi utilitas untuk memanipulasi string, antara lain:

- `strcopy`: Menyalin string dengan aman menggunakan `strncpy`.

- `find_strcase`: Mencari keberadaan sebuah string dalam daftar string dengan memperhatikan perbedaan huruf besar dan kecil.

- `get_tokens`: Membagi string input menjadi token-token terpisah berdasarkan delimiter yang diberikan.

- `shift_left`: Menggeser isi string ke kiri.

### **Utilitas Lainnya**

Ada beberapa fungsi utilitas lain yang digunakan dalam program ini, antara lain:

- `get_path`: Menetapkan nilai untuk `databases_path` berdasarkan direktori saat ini.

- `get_timestamp`: Mengambil timestamp (waktu saat ini) dalam format yang sesuai.

- `initiate_database`: Menginisialisasi database utama.

- `check_user`: Memeriksa apakah pengguna dengan nama yang diberikan sudah ada.

- `check_user_password`: Memeriksa apakah nama pengguna ada dan apakah password nya benar.

- `check_arguments`: Memeriksa argumen-argumen yang dimasukkan ketika menjalankan program client atau dump.

- `check_user_login`: Memeriksa nama pengguna, password, sekaligus argumen untuk tipe pengguna "user".

- `check_db`: Memeriksa apakah database dengan nama yang diberikan sudah ada.

- `check_permission`: Memeriksa izin pengguna terhadap sebuah database.

- `check_db_permission`: Menjalankan `check_db` dan `check_permission`.

- `check_table`: Memeriksa apakah tabel dengan nama yang diberikan sudah ada di dalam sebuah database.

- `value_type`: Memeriksa tipe data dari sebuah nilai dalam kolom.

- `get_columns`: Mendapatkan kolom-kolom dan tipe data dari sebuah tabel.

- `find_column`: Mencari indeks kolom dalam daftar kolom.

- `select_output`: Menulis output dari perintah select ke variabel output.

- `get_tables`: Mendapatkan atribut-atribut serta data-data yang ada pada suatu tabel.

### **Penanganan Perintah**

Ada beberapa fungsi yang menangani berbagai jenis perintah yang diterima oleh server, antara lain:

- `handle_login`: Fungsi untuk menangani perintah LOGIN ketika client baru saja menjalankan program.

- `handle_dump`: Fungsi untuk menangani perintah DUMP ketika client ingin membuat backup dari suatu database.

- `handle_read`: Fungsi untuk menangani perintah READ ketika program client membaca input dari suatu file.

- `handle_create`: Fungsi untuk menangani perintah CREATE yang berbeda.

- `handle_create_user`: Fungsi untuk menangani pembuatan pengguna.

- `handle_create_database`: Fungsi untuk menangani pembuatan database.

- `handle_create_table`: Fungsi untuk menangani pembuatan tabel.

- `handle_use`: Fungsi untuk menangani perintah USE (memilih database).

- `handle_grant_permission`: Fungsi untuk menangani perintah memberikan izin.

- `handle_drop`: Fungsi untuk menangani perintah DROP yang berbeda.

- `handle_drop_database`: Fungsi untuk menangani penghapusan database.

- `handle_drop_table`: Fungsi untuk menangani penghapusan tabel.

- `handle_drop_column`: Fungsi untuk menangani penghapusan kolom.

- `handle_insert`: Fungsi untuk menangani perintah INSERT.

- `handle_delete`: Fungsi untuk menangani perintah DELETE.

- `handle_update`: Fungsi untuk menangani perintah UPDATE.

- `handle_select`: Fungsi untuk menangani perintah SELECT.

- `parse_command`: Fungsi untuk mem-*parsing* perintah yang diterima.

### **Fungsi Utama**

Fungsi utama (`main`) melakukan inisialisasi server, pengaturan koneksi, dan menjalankan server untuk menerima dan memproses perintah dari klien.

Fungsi utama melakukan hal-hal berikut:

1. Membuka socket server dan mengikatkannya ke alamat dan port yang ditentukan.

2. Memulai database dan menginisialisasi variabel-variabel terkait.

3. Membuka semaphore untuk mengatur akses ke database.

4. Menerima koneksi masuk dari klien.

5. Menjalankan proses anak untuk menangani koneksi dengan klien.

6. Di dalam proses anak, menerima perintah dari klien, memproses perintah tersebut, dan mengirimkan output kembali ke klien.

7. Jika perintah `USE` atau `READ` berhasil dijalankan, memperbarui database saat ini.

8. Jika perintah `LOGIN`, `DUMP`, atau `READ` gagal, keluar dari proses anak.

9. Jika perintah adalah `EXIT` atau `DUMP`, keluar dari proses anak.

10. Mengulangi langkah 4-8 untuk menerima dan memproses perintah dari klien secara berkelanjutan.

### **Detail**

#### **String Utilities**

```c
void strcopy(char* str1, char* str2, size_t bufsize) {
    strncpy(str1, str2, bufsize);
    if (str1[bufsize - 1] != '\0')
        str1[bufsize] = '\0';
}
```

Fungsi `strcopy` digunakan untuk menyalin string `str2` ke `str1` dengan menggunakan fungsi `strncpy`. Fungsi ini juga memastikan bahwa string `str1` diakhiri dengan karakter null (`\0`) jika ukuran buffer masih cukup.

```c
int find_strcase(char *str, char **list, size_t size) {
    for (int i = 0; i < size; i++) {
        if (list[i] == NULL)
            break;
        if (strcasecmp(str, list[i]) == 0)
            return i;
    }
    return -1;
}
```

Fungsi `find_strcase` digunakan untuk mencari keberadaan sebuah string (`str`) dalam daftar string (`list`) dengan memperhatikan perbedaan huruf besar dan kecil. Fungsi ini melakukan pencarian secara tidak peka terhadap huruf besar/kecil dan mengembalikan indeks dari string yang cocok. Jika string tidak ditemukan, fungsi ini mengembalikan nilai -1.

```c
void shift_left(char *str) {
    for (int i = 0; str[i] != '\0'; i++) {
        str[i] = str[i + 1];
    }
}
```

Fungsi `shift_left` digunakan untuk menggeser string ke kiri dengan menghapus karakter pertama. Fungsi ini berguna untuk menghilangkan tanda kutip tunggal pada string yang diapit oleh tanda kutip dalam perintah.

```c
int get_tokens(char *buffer, char **tokens, char *delimiters) {
    int i = 0;
    char *after_quote = NULL;
    tokens[i] = strtok(buffer, delimiters);
    while (tokens[i] != NULL) {
        // Get quoted token
        if (tokens[i][0] == '\'') {
            tokens[i][strlen(tokens[i])] = ' ';
            for (int j = 1; tokens[i][j] != '\0'; j++) {
                if (tokens[i][j] == '\'') {
                    // Handle escaped quote
                    if (tokens[i][j + 1] == '\'') {
                        shift_left(tokens[i] + j);
                        j++;
                        continue;
                    }
                    tokens[i][j + 1] = '\0';
                    after_quote = &tokens[i][j + 2];
                    break;
                }
            }
        }
        // Get next token
        i++;
        if (after_quote) {
            tokens[i] = strtok(after_quote, delimiters);
            after_quote = NULL;
        } else {
            tokens[i] = strtok(NULL, delimiters);
        }
    }
    return i;
}
```

Fungsi `get_tokens` digunakan untuk membagi string input menjadi token-token yang terpisah berdasarkan delimiter yang ditentukan. Fungsi ini mengembalikan jumlah token yang berhasil dibaca. Fungsi ini juga mendukung token yang diapit oleh tanda kutip tunggal (`'`) dan dapat mengenali karakter kutip ganda yang di-escape dalam string.

#### **More Utilities**

```c
void get_path()
{
    // ...
}
```
Fungsi `get_path` digunakan untuk mendapatkan jalur direktori saat ini dengan menggunakan perintah sistem `pwd` pada sistem operasi. Fungsi ini membaca jalur dari file `temp.txt`, membersihkan karakter newline (`\n`), dan menyimpan jalur tersebut ke dalam variabel `databases_path`.

```c
char *get_timestamp()
{
    // ...
}
```

Fungsi `get_timestamp` digunakan untuk mengambil timestamp saat ini dengan menggunakan fungsi `time` dan `localtime` untuk mendapatkan informasi waktu lokal. Fungsi ini mengembalikan timestamp dalam format string `YYYY-MM-DD HH:MM:SS`.

```c
void write_log(const char *username, const char *cmd)
{
    // ...
}
```

Fungsi `write_log` digunakan untuk menulis log ke file `log_path`. Fungsi ini membuka file dalam mode append (`"a"`) dan menulis timestamp, username, dan perintah ke dalam file tersebut. Setelah selesai, file ditutup kembali.

```c
void initiate_database(char *database)
{
    // ...
}
```
Fungsi `initiate_database` digunakan untuk menginisialisasi database dengan membuat direktori untuk databases, dan membuat file `users.txt` dan `log.txt` dalam direktori databases. Fungsi ini juga mengatur jalur `users_path` dan `log_path` sesuai dengan jalur direktori databases.

```c
int check_user(char *username) {
    // ...
}
```

Fungsi `check_user` digunakan untuk memeriksa apakah pengguna dengan `username` tertentu ada dalam file `users.txt`. Fungsi ini membuka file `users.txt`, membaca baris per baris, dan membandingkan dengan `username` yang diberikan. Jika pengguna ditemukan, fungsi ini mengembalikan nilai 1; jika tidak, mengembalikan nilai 0.

```c
int check_user_password(char *username, char *password){
    // ...
}
```

Fungsi `check_user_password` digunakan untuk memeriksa apakah pengguna dengan `username` tertentu ada dalam file `users.txt`. Jika ada, maka kemudian akan dicek apakah `password` dari `username` tersebut benar. Fungsi ini membuka file `users.txt`, membaca baris per baris, dan membandingkan dengan `username` yang diberikan. Jika file tidak bisa dibaca kembalikan nilai -1, jika pengguna tidak ditemukan kembalikan nilai 0, jika password salah kembalikan nilai 1, jika pengguna ditemukan dan password benar kembalikan nilai 2.

```c
int check_arguments(int args, int num, char *output, int failed, int success) {
    // ...
}
```

Fungsi ini menerima banyaknya argumen yang terbaca ketika menjalankan program client atau dump, serta banyaknya argumen yang seharusnya dimasukkan. Kemudian akan dibandingkan, apakah terlalu sedikit atau terlalu banyak. Fungsi akan mengambalikan nilai `failed` atau `success` tergantung dari fungsi yang memanggil fungsi ini.

```c
int check_user_login(char **tokens, char *output, int num_tokens, int failed, int success, int dumpFlag) {
    // ...
}
```

Fungsi ini menerima token yang didapatkan berdasarkan perintah yang dimasukkan oleh client. Kemudian mengecek argumen dengan memanggil `check_arguments`, serta mengecek pengguna dan password dengan memanggil `check_user_password`.

```c
int check_db(char *db_name) {
    // ...
}
```

Fungsi `check_db` digunakan untuk memeriksa apakah sebuah database dengan nama `db_name` ada dalam direktori `databases`. Fungsi ini menggunakan perintah sistem `ls` untuk mendapatkan daftar file/direktori dalam `databases` dan membandingkan dengan `db_name`. Jika database ditemukan, fungsi ini mengembalikan nilai 1; jika tidak, mengembalikan nilai 0.

```c
int check_permission(char *db_name, char *user_name)
{
    // ...
}
```

Fungsi `check_permission` digunakan untuk memeriksa izin pengguna (`user_name`) terhadap sebuah database (`db_name`). Fungsi ini membaca file `permission.txt` dari direktori database yang bersangkutan dan memeriksa apakah `user_name` terdaftar dalam file tersebut. Jika pengguna memiliki izin, fungsi ini mengembalikan nilai 1; jika tidak, mengembalikan nilai 0.

```c
int check_db_permission(char *db_name, char *user_name, char *output, int failed, int success) {
    // ...
}
```

Fungsi `check_db_permission` memanggil fungsi `check_db` dan `check_permission`. Berguna supaya tidak ada redundan pada fungsi-fungsi yang memanggil kedua fungsi tersebut.

```c
int check_table(char *db_name, char *table_name) {
    // ...
}
```

Fungsi `check_table` digunakan untuk memeriksa apakah sebuah tabel (`table_name`) ada dalam database (`db_name`). Fungsi ini menggunakan perintah sistem `ls`

 untuk mendapatkan daftar file/direktori dalam direktori database yang bersangkutan dan membandingkan dengan `table_name`. Jika tabel ditemukan, fungsi ini mengembalikan nilai 1; jika tidak, mengembalikan nilai 0.

```c
const char* value_type(char *value) {
    // ...
}
```

Fungsi `value_type` digunakan untuk menentukan tipe data dari nilai (`value`) dalam string. Fungsi ini melakukan pemeriksaan karakter pertama dan terakhir dari nilai untuk menentukan tipe datanya. Jika nilai diapit oleh tanda kutip tunggal (`'`), maka tipe datanya adalah string. Jika nilai tidak diapit oleh tanda kutip tunggal dan bukan angka, tipe datanya adalah `unknown`. Jika nilai merupakan angka atau `0`, tipe datanya adalah `int`.

```c
int get_columns(char *table_path, char **columns) {
    // ...
}
```

Fungsi `get_columns` digunakan untuk mendapatkan daftar kolom dari sebuah tabel. Fungsi ini membaca baris pertama dari file tabel yang diberikan (`table_path`) dan membaginya menjadi token-token kolom menggunakan fungsi `get_tokens`. Fungsi ini mengembalikan jumlah token yang berhasil dibaca.

```c
int find_column(char **columns, char* column, size_t num_tokens) {
    // ...
}
```

Fungsi `find_column` digunakan untuk mencari posisi kolom dalam daftar kolom (`columns`) berdasarkan nama kolom (`column`). Fungsi ini menggunakan fungsi `find_strcase` untuk melakukan pencarian yang tidak case-sensitive. Jika kolom ditemukan, fungsi ini mengembalikan nomor kolom; jika tidak, mengembalikan nilai 0.

```c
void select_output(char *output)
{
    // ...
}
```

Fungsi `select_output` digunakan untuk menggabungkan semua baris output dari sebuah perintah select menjadi satu string dalam variabel `output`. Fungsi ini membaca file `temp` baris per baris, dan setiap barisnya ditambahkan ke dalam `output`. Terakhir, karakter newline (`\n`) dihapus dari akhir string `output`.


#### **Handle Commands**

```c
int handle_login(char **tokens, char *output, int num_tokens) {
    // ...
}
```

Fungsi `handle_login` digunakan sebagai autentikasi ketika client baru saja menjalankan program_client. Fungsi menerima token-token yang didapatkan, kemudian memanggil `check_user_login` untuk melakukan pengecekan. Kemudian fungsi akan mengembalikan nilai 4 jika autentikasi gagal, serta mengembalikan nilai 1 jika autentikasi berhasil.

```c
int handle_dump(char **tokens, char *output, int num_tokens) {
    // ...
}
```

Fungsi `handle_dump` digunakan ketika client ingin melakukan backup suatu database melalui program_dump. Fungsi menerima token-token yang didapatkan, kemudian memanggil `check_user_login` untuk melakukan autentikasi. Jika autentikasi berhasil, maka lanjut memanggil `check_db_permission` untuk mengecek apakah database ada dan apakah pengguna memiliki hak akses pada database tersebut. Jika berhasil, dilanjutkan dengan membaca seluruh tabel pada database tersebut, beserta atribut-atribut dan record-record nya. Setiap proses gagal mengembalikan nilai 5, apabila berhasil mengambalikan nilai 6.

```c
int handle_read(char **tokens, char *output, int num_tokens) {
    // ...
}
```

Fungsi `handle_read` digunakan ketika program_client menerima input dari suatu file backup. Fungsi menerima token-token yang didapatkan, kemudian memanggil `check_user_login` untuk melakukan autentikasi. Jika autentikasi berhasil, maka lanjut memanggil `check_db_permission` untuk mengecek apakah database ada dan apakah pengguna memiliki hak akses pada database tersebut. Jika berhasil maka kembalikan nilai 8, jika gagal maka kembalikan nilai 7.

```c
int handle_create(char **tokens, char *output) {
    if(tokens[SUB_CMD_IDX] == NULL) {
        printf("Expected USER|DATABASE|TABLE\n");
        strcopy(output, "Expected USER|DATABASE|TABLE", OUT_BUFFER);
        return 0;
    }
    int result = 0;

    char *sublist[] = {"USER", "DATABASE", "TABLE"};
    switch(find_strcase(tokens[SUB_CMD_IDX], sublist, 3)) {
        case 0:
            printf("CREATE USER command\n");
            result = handle_create_user(tokens, output);
            break;
        case 1:
            printf("CREATE DATABASE command\n");
            result = handle_create_database(tokens, output);
            break;
        case 2:
            printf("CREATE TABLE command\n");
            result = handle_create_table(tokens, output);
            break;
        default:
            printf("Expected USER|DATABASE|TABLE\n");
            strcopy(output, "Expected USER|DATABASE|TABLE", OUT_BUFFER);
            result = 0;
            break;
    }
    return result;
}
```

Fungsi `handle_create` adalah fungsi utama yang menangani perintah "CREATE". Fungsi ini memeriksa argumen sub-perintah yang diberikan dalam array `tokens`. Jika argumen sub-perintah tidak ada (NULL), maka pesan kesalahan "Expected USER|DATABASE|TABLE" akan dicetak dan disalin ke variabel `output`. Fungsi ini juga menggunakan fungsi `find_strcase` untuk mencari tahu jenis perintah yang dimasukkan (USER, DATABASE, atau TABLE) dan memanggil fungsi yang sesuai untuk menangani perintah tersebut.

```c
int handle_create_user(char **tokens, char* output) {
    if(strcmp(tokens[0], "root") != 0) {
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    // ...
}
```

Fungsi `handle_create_user` menangani perintah "CREATE USER". Fungsi ini memeriksa apakah pengguna yang menjalankan perintah adalah pengguna "root". Jika bukan, pesan kesalahan "Permission denied!" akan dicetak dan disalin ke variabel `output`. Jika pengguna adalah "root", fungsi ini melanjutkan memeriksa apakah argumen username telah diberikan. Jika tidak, pesan kesalahan "Expected username!" akan dicetak dan disalin ke variabel `output`. Jika argumen username telah diberikan, fungsi ini memeriksa keberadaan pengguna dengan memanggil fungsi `check_user`. Jika pengguna sudah ada, pesan kesalahan "User exists!" akan dicetak. Jika semuanya valid, pengguna baru ditambahkan ke file "users.txt".

```c
int handle_create_database(char **tokens, char *output) {
    // no database name
    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected Database Name!\n");
        strcopy(output, "Expected Database Name!", OUT_BUFFER);
        return 0;
    }

    // ...
}
```

Fungsi `handle_create_database` menangani perintah "CREATE DATABASE". Fungsi ini memeriksa apakah argumen nama database telah diberikan. Jika tidak, pesan kesalahan "Expected Database Name!" akan

 dicetak dan disalin ke variabel `output`. Jika argumen nama database telah diberikan, fungsi ini melanjutkan dengan memeriksa keberadaan database dengan memanggil fungsi `check_database`. Jika database sudah ada, pesan kesalahan "Database exists!" akan dicetak. Jika semuanya valid, fungsi ini membuat direktori baru untuk database yang dimaksud.

```c
int handle_create_table(char **tokens, char *output) {
    // no table name
    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    // ...
}
```

Fungsi `handle_create_table` menangani perintah "CREATE TABLE". Fungsi ini memeriksa apakah argumen nama tabel telah diberikan. Jika tidak, pesan kesalahan "Expected Table Name!" akan dicetak dan disalin ke variabel `output`. Jika argumen nama tabel telah diberikan, fungsi ini melanjutkan dengan memeriksa keberadaan database dan izin pengguna dengan memanggil fungsi `check_database` dan `check_permission`. Jika ada kesalahan, pesan kesalahan yang sesuai akan dicetak. Jika semuanya valid, fungsi ini membuat file baru untuk tabel yang dimaksud.

```c
int handle_use(char **tokens, char *output) {
    // no database name
    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected Database Name!\n");
        strcopy(output, "Expected Database Name!", OUT_BUFFER);
        return 0;
    }

    // ...
}
```

Fungsi `handle_use` menangani perintah "USE" untuk mengganti database yang sedang digunakan. Fungsi ini memeriksa apakah argumen nama database telah diberikan. Jika tidak, pesan kesalahan "Expected Database Name!" akan dicetak dan disalin ke variabel `output`. Jika argumen nama database telah diberikan, fungsi ini melanjutkan dengan memeriksa keberadaan database dengan memanggil fungsi `check_database`. Jika database tidak ada, pesan kesalahan "Database not found!" akan dicetak. Jika semuanya valid, fungsi ini mengubah database yang sedang digunakan.

```c
int handle_grant_permission(char **tokens, char* output) {
    // permission target is not provided
    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected Database/User Name!\n");
        strcopy(output, "Expected Database/User Name!", OUT_BUFFER);
        return 0;
    }

    // ...
}
```

Fungsi `handle_grant_permission` menangani perintah "GRANT PERMISSION". Fungsi ini memeriksa apakah argumen target izin (database/pengguna) telah diberikan. Jika tidak, pesan kesalahan "Expected Database/User Name!" akan dicetak dan disalin ke variabel `output`. Jika argumen target izin telah diberikan, fungsi ini melanjutkan dengan memeriksa izin pengguna "root" dengan membandingkannya dengan pengguna yang menjalankan perintah. Jika bukan "root", pesan kesalahan "Permission denied!" akan dicetak. Jika semuanya valid, izin pengguna ditambahkan ke file "permission.txt".

```c
int handle_drop(char **tokens, char *output) {
    if(tokens[SUB_CMD_IDX] == NULL) {
        printf("Expected DATABASE|TABLE|COLUMN\n");
        strcopy(output, "Expected DATABASE|TABLE|COLUMN", OUT_BUFFER);
        return 0;
    }
    int result = 0;

    char *sublist[] = {"DATABASE", "TABLE", "COLUMN"};
    switch(find_strcase(tokens[SUB_CMD_IDX], sublist, 3)) {
        case 0:
            printf("DROP DATABASE command\n");
            result = handle_drop_database(tokens, output);
            break;
        case 1:
            printf("DROP TABLE command\n");
            result = handle_drop_table(tokens, output);
            break;
        case 2:
            printf("DROP COLUMN command\n");
            result = handle_drop_column(tokens, output);
            break;
        default:
            printf("Expected DATABASE|TABLE|COLUMN\n");
            strcopy(output, "Expected DATABASE|TABLE|COLUMN", OUT_BUFFER);
            result = 0;
            break;
    }
    return result;
}
```

Fungsi `handle_drop` menangani perintah "DROP" dengan memeriksa argumen yang diberikan untuk menentukan apakah itu adalah "DATABASE", "TABLE", atau "COLUMN". Fungsi ini menggunakan `find_strcase` untuk mencari kata kunci yang cocok di antara sublist yang ditentukan. Jika argumen tidak cocok dengan kata kunci yang valid, fungsi ini akan mencetak pesan kesalahan dan mengembalikan nilai 0. Jika argumen cocok, fungsi akan memanggil fungsi yang sesuai untuk menangani perintah "DROP" yang spesifik (misalnya `handle_drop_database`, `handle_drop_table`, atau `handle_drop_column`) dan mengembalikan hasilnya.

```c
int handle_drop_database(char **tokens, char *output) {
    // no database name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Database Name!\n");
        strcopy(output, "Expected Database Name!", OUT_BUFFER);
        return 0;
    }

    //check db and user permission
    if(!check_db(tokens[SUB_CMD_IDX + 1])) {
        printf("Database not found!\n");
        strcopy(output, "Database not found!", OUT_BUFFER);
        return 0;
    }

    if(!check_permission(tokens[SUB_CMD_IDX + 1], tokens[0])) {
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    // drop database
    char cmdDB[CMD_BUFFER];
    snprintf(cmdDB, sizeof(cmdDB), "rm -r %s/databases/%s", databases_path, tokens[SUB_CMD_IDX + 1]);
    system(cmdDB);
    return 1;
}
```

Fungsi `handle_drop_database` menangani perintah "DROP DATABASE". Fungsi ini memeriksa apakah argumen nama database telah diberikan. Jika tidak, pesan kesalahan "Expected Database Name!" akan dicetak dan disalin ke variabel `output`. Jika argumen nama database telah diberikan, fungsi ini melanjutkan dengan memeriksa keberadaan database dan izin pengguna dengan memanggil fungsi `check_db` dan `check_permission`. Jika database tidak ditemukan, pesan kesalahan "Database not found!" dicetak dan disalin ke variabel `output`. Jika izin ditolak, pesan kesalahan "Permission denied!" dicetak dan disalin ke variabel `output`. Jika semuanya valid, fungsi ini akan menjalankan perintah sistem untuk menghapus database.

```c
int handle_drop_table(char **tokens, char *output) {
    // no table name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    //check db and user permission
    if(!check_db(tokens[1])) {
        printf("Database not found!\n");
        strcopy(output, "Database not found!", OUT_BUFFER);
        return 0;
    }

    if(!check_permission(tokens[1], tokens[0])) {
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    // check table not found
    if(!check_table(tokens[1], tokens[SUB_CMD_IDX + 1])){
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    // drop table
    char cmdTable[CMD_BUFFER];
    snprintf(cmdTable, sizeof(cmdTable), "rm %s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 1]);
    system(cmdTable);
    return 1;
}
```

Fungsi `handle_drop_table` menangani perintah "DROP TABLE". Fungsi ini memeriksa apakah argumen nama tabel telah diberikan. Jika tidak, pesan kesalahan "Expected Table Name!" akan dicetak dan disalin ke variabel `output`. Jika argumen nama tabel telah diberikan, fungsi ini melanjutkan dengan memeriksa keberadaan database, izin pengguna, dan keberadaan tabel dengan memanggil fungsi `check_db`, `check_permission`, dan `check_table`. Jika database tidak ditemukan, pesan kesalahan "Database not found!" dicetak dan disalin ke variabel `output`. Jika izin ditolak, pesan kesalahan "Permission denied!" dicetak dan disalin ke variabel `output`. Jika tabel tidak ditemukan, pesan kesalahan "Table not found!" dicetak dan disalin ke variabel `output`. Jika semuanya valid, fungsi ini akan menjalankan perintah sistem untuk menghapus tabel.

```c
int handle_drop_column(char **tokens, char *output) {
    // no column name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Column Name!\n");
        strcopy(output, "Expected Column Name!", OUT_BUFFER);
        return 0;
    }
    
    // invalid syntax
    if(tokens[SUB_CMD_IDX + 2] == NULL){
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }
    if(strcasecmp(tokens[SUB_CMD_IDX + 2], "FROM") != 0){
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    // no table name
    if(tokens[SUB_CMD_IDX + 3] == NULL){
        printf("Expected Table Name!\n");
       

 strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    // check db and user permission
    if(!check_db(tokens[1])){
        printf("Database not found!\n");
        strcopy(output, "Database not found!", OUT_BUFFER);
        return 0;
    }
    if(!check_permission(tokens[1], tokens[0])){
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    // check table not found
    if(!check_table(tokens[1], tokens[SUB_CMD_IDX + 3])){
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    // table path
    char tablePath[STR_BUFFER];
    snprintf(tablePath, sizeof(tablePath), "%s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 3]);

    // target column
    char column[STR_BUFFER];
    strcopy(column, tokens[SUB_CMD_IDX + 1], STR_BUFFER);

    // Get columns and datatype for said columns
    char *columns[MAX_TOKENS];
    int num_columns = get_columns(tablePath, columns);

    // Get column number
    int collnum = find_column(columns, column, num_columns);
    if(collnum == 0){
        printf("Column not found!\n");
        strcopy(output, "Column not found!", OUT_BUFFER);
        return 0;
    }
    collnum >>= 1;

    FILE *fp = fopen(tablePath, "r");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    char line[STR_BUFFER];
    char str_result[CMD_BUFFER] = "";

    while(fgets(line, sizeof(line), fp) != NULL)
    {
        line[strcspn(line, "\n")] = 0;

        char buffer[STR_BUFFER];
        strcopy(buffer, line, STR_BUFFER);

        char *tokenss[256];
        int num_tokenss = get_tokens(buffer, tokenss, " (),;=\n");

        for(int i = 0; i < num_tokenss; ++i){
            if(i == collnum)
                continue;
            else {
                if(i > 0){
                    if(!(i == 1 && collnum == 0))
                        strcat(str_result, " ");
                }
                strcat(str_result, tokenss[i]);
            }
        }
        strcat(str_result, "\n");
    }
    fclose(fp);

    fp = fopen(tablePath, "w");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    fprintf(fp, "%s", str_result);
    fclose(fp);
    return 1;
}
```

Fungsi `handle_drop_column` menangani perintah "DROP COLUMN". Fungsi ini memeriksa apakah argumen nama kolom, sintaks yang valid, dan nama tabel telah diberikan. Jika salah satu dari mereka tidak ditemukan, pesan kesalahan yang sesuai dicetak dan disalin ke variabel `output`. Fungsi ini juga memeriksa keberadaan database, izin pengguna, dan keberadaan tabel dengan memanggil fungsi `check_db`, `check_permission`, dan `check_table`. Jika ada kesalahan dalam salah satu langkah ini, pesan kesalahan yang sesuai akan dicetak dan disalin ke variabel `output`. Jika semuanya valid, fungsi ini membuka file tabel, membaca setiap baris, dan menghapus kolom yang sesuai. Kemudian, data yang diubah ditulis kembali ke file tabel.

```c
int handle_insert(char ** tokens, char *output, int num_tokens) {
    // ...
}
```

Fungsi ini menghandle perintah `INSERT` untuk memasukkan data baru ke dalam tabel. Fungsi ini menerima token-token perintah yang telah dipisahkan, buffer output, dan jumlah token. Fungsi ini melakukan beberapa validasi, seperti memeriksa keberadaan nama database, izin pengguna, dan keberadaan tabel yang dimaksud. Selanjutnya, data yang akan dimasukkan diambil dari token-token yang diberikan, kemudian ditulis ke dalam file tabel menggunakan `fprintf`. Jika berhasil, fungsi akan mengembalikan nilai 1, jika tidak, akan mengembalikan nilai 0.

```c
int handle_delete(char **tokens, char* output) {
    // ...
}
```
Fungsi ini menghandle perintah `DELETE` untuk menghapus data dari tabel. Fungsi ini menerima token-token perintah yang telah dipisahkan dan buffer output. Fungsi ini melakukan validasi, seperti memeriksa keberadaan database, izin pengguna, dan keberadaan tabel yang dimaksud. Jika perintah `DELETE` hanya diikuti dengan nama tabel, fungsi akan menghapus semua baris data dari tabel tersebut. Jika perintah `DELETE` diikuti dengan klausa `WHERE`, fungsi akan memeriksa keberadaan kolom yang dimaksud dan tipe datanya. Jika semua validasi berhasil, fungsi akan menggunakan perintah `awk` untuk menghapus baris-baris data yang sesuai dengan kondisi yang diberikan. Jika berhasil, fungsi akan mengembalikan nilai 1, jika tidak, akan mengembalikan nilai 0.

```c
int handle_update(char **tokens, char* output) {
    // ...
}
```

Fungsi ini menghandle perintah `UPDATE` untuk mengubah data dalam tabel. Fungsi ini menerima token-token perintah yang telah dipisahkan dan buffer output. Fungsi ini melakukan validasi, seperti memeriksa keberadaan database, izin pengguna, dan keberadaan tabel yang dimaksud. Jika perintah `UPDATE` diikuti dengan klausa `SET`, fungsi akan memeriksa keberadaan kolom yang dimaksud dan tipe datanya. Jika perintah `UPDATE` hanya diikuti dengan nama tabel dan klausa `SET`, fungsi akan mengubah semua baris data dalam tabel tersebut. Jika perintah `UPDATE` diikuti dengan klausa `WHERE`, fungsi akan memeriksa keberadaan kolom yang dimaksud dan tipe datanya. Jika semua validasi berhasil, fungsi akan menggunakan perintah `awk` untuk mengubah baris-baris data yang sesuai dengan kondisi yang diberikan. Jika berhasil, fungsi akan mengembalikan nilai 1, jika tidak, akan mengembalikan nilai 0.

```c
int handle_select(char **tokens, char* output) {
    // ...
}
```
Fungsi ini menghandle perintah `SELECT` untuk mengambil data dari tabel. Fungsi ini menerima token-token perintah yang telah dipisahkan dan buffer output. Fungsi ini melakukan validasi, seperti memeriksa keberadaan database dan izin pengguna. Selanjutnya, fungsi mencari file tabel yang dimaksud dan membaca baris-baris data dari file tersebut menggunakan `fgets`. Jika perintah `SELECT` hanya diikuti dengan nama tabel, fungsi akan mengambil semua baris data dari tabel tersebut. Jika perintah `SELECT` diikuti dengan klausa `WHERE`, fungsi akan memeriksa keberadaan kolom yang dimaksud dan tipe datanya. Jika semua validasi berhasil, fungsi akan menggunakan perintah `awk` untuk mengambil baris-baris data yang sesuai dengan kondisi yang diberikan. Data yang ditemukan akan disimpan dalam buffer output. Jika berhasil, fungsi akan mengembalikan nilai 1, jika tidak, akan mengembalikan nilai 0.

```c
int parse_command(char *cmd, char* output) {
    char *tokens[256];
    char buffer[CMD_BUFFER];

    strcopy(buffer, cmd, CMD_BUFFER);
    int num_tokens = get_tokens(buffer, tokens, " (),;=\n");

    // debug command
    for(int i = 0; i < num_tokens; i++) {
        printf("--> %s\n", tokens[i]);
    }

    // no username
    if(tokens[0] == NULL) {
        printf("User not found!\n");
        return 0;
    }
    // no command
    if(tokens[CMD_IDX] == NULL) {
        printf("Command not found!\n");
        return 0;
    }

    // Write log
    int start_cmd = strlen(tokens[0]) + strlen(tokens[1]) + 2;
    write_log(tokens[0], cmd + start_cmd);

    int result = 0;

    char *sublist[] = {"USE", "CREATE", "GRANT", "DROP", "INSERT", "UPDATE", "DELETE", "SELECT", "EXIT", "LOGIN", "DUMP", "READ"};
    switch(find_strcase(tokens[CMD_IDX], sublist, 12)) {
        case 0:
            printf("USE command\n");
            // Handle use
            // result = 0 (failed)
            // result = 2 (success)
            result = handle_use(tokens, output);
            if(result == 2)
                strcopy(cmd, tokens[SUB_CMD_IDX], CMD_BUFFER);
            break;
        case 1:
            printf("CREATE command\n");
            // Handle create
            result = handle_create(tokens, output);
            break;
        case 2:
            printf("GRANT command\n");
            // Handle grant
            result = handle_grant_permission(tokens, output);
            break;
        case 3:
            printf("DROP command\n");
            // Handle drop
            result = handle_drop(tokens, output);
            break;
        case 4:
            printf("INSERT command\n");
            // Handle insert
            result = handle_insert(tokens, output, num_tokens);
            break;
        case 5:
            printf("UPDATE command\n");
            // Handle update
            result = handle_update(tokens, output);
            break;
        case 6:
            printf("DELETE command\n");
            // Handle delete
            result = handle_delete(tokens, output);
            break;
        case 7:
            printf("SELECT command\n");
            // Handle select
            result = handle_select(tokens, output);
            break;
        case 8:
            printf("EXIT command\n");
            result = 3;
            break;
        case 9:
            printf("LOGIN command\n");
            result = handle_login(tokens, output, num_tokens);
            break;
        case 10:
            printf("DUMP command\n");
            result = handle_dump(tokens, output, num_tokens);
            break;
        case 11:
            printf("READ command\n");
            // result = 7 (failed)
            // result = 8 (success)
            result = handle_read(tokens, output, num_tokens);
            if(result == 8)
                strcopy(cmd, tokens[SUB_CMD_IDX + 5], CMD_BUFFER);
            break;
        default:
            printf("Invalid Syntax!\n");
            strcopy(output, "Invalid Syntax!", OUT_BUFFER);
            result = 0;
            break;
    }
    return result;
}
```

Fungsi `parse_command` memiliki fungsi untuk mem-parsing dan meng-handle perintah yang diberikan dalam bentuk string `cmd`. Berikut adalah penjelasan langkah-langkah yang dilakukan oleh fungsi ini:

- Pertama, fungsi mendeklarasikan variabel lokal sebagai berikut:
  - `tokens`: array pointer yang akan digunakan untuk menyimpan token-token yang dipisahkan dari perintah.
  - `buffer`: array karakter yang berfungsi sebagai buffer untuk menyalin isi string `cmd`.
- Selanjutnya, fungsi `strcopy` digunakan untuk menyalin isi string `cmd` ke dalam `buffer` menggunakan fungsi `strcopy

`.
- Fungsi `get_tokens` dipanggil untuk memisahkan `buffer` menjadi token-token berdasarkan delimiter yang ditentukan. Jumlah token yang dihasilkan disimpan dalam variabel `num_tokens`.
- Selanjutnya, terdapat perulangan `for` yang digunakan untuk melakukan debugging. Token-token yang dihasilkan akan ditampilkan dengan menggunakan fungsi `printf`.
- Setelah itu, dilakukan validasi sebagai berikut:
  - Jika `tokens[0]` (username) tidak ada, maka akan ditampilkan pesan "User not found!" dan fungsi akan mengembalikan nilai 0.
  - Jika `tokens[CMD_IDX]` (token perintah) tidak ada, maka fungsi akan mengembalikan nilai 0.
- Setelah validasi, dilakukan penulisan log menggunakan fungsi `write_log`. Indeks awal token perintah dihitung dengan menjumlahkan panjang `tokens[0]`, panjang `tokens[1]`, dan 2 (untuk mengabaikan spasi dan karakter pemisah).
- Kemudian, variabel `result` diinisialisasi dengan nilai 0.
- Selanjutnya, terdapat sebuah `switch` yang digunakan untuk memeriksa token perintah dengan menggunakan fungsi `find_strcase`. Terdapat 9 kemungkinan perintah yang akan diperiksa, yaitu "USE", "CREATE", "GRANT", "DROP", "INSERT", "UPDATE", "DELETE", "SELECT", dan "EXIT".
- Setiap kasus di dalam `switch` akan menampilkan pesan sesuai dengan perintah yang ditemukan dan akan memanggil fungsi penanganan yang sesuai dengan perintah tersebut.
- Fungsi penanganan akan menghasilkan nilai yang akan disimpan dalam variabel `result`.
- Terdapat juga kondisi khusus untuk perintah "USE", di mana jika `result` adalah 2, maka `cmd` akan disalin dengan token sub-perintah menggunakan fungsi `strcopy`.
- Jika perintah tidak cocok dengan kasus manapun, akan ditampilkan pesan "Invalid Syntax!" dan nilai `result` akan diset menjadi 0.


### **Cara Menggunakan**

1. Compile program server menggunakan kompilator C seperti GCC.
2. Jalankan program server yang telah dikompilasi.
3. Pastikan server berjalan dan dapat menerima koneksi masuk.
4. Compile dan jalankan program client pada terminal lain.
5. Masukkan perintah ke client dan perintah akan dikirim ke server untuk dieksekusi.
6. Server akan memberikan respons ke client berdasarkan hasil eksekusi perintah.

<br>

## **Containerization**

Untuk menguji kemampuan multi-client server, akan mengunnakan containerization dengan Docker. Beberapa hal yang diperlukan adalah :
1. ``Dockerfile`` untuk menjalankan command-command yang digunakan untuk menyusun sebuah Docker Image
2. ``docker-compose.yml`` untuk mendefinisikan container-container dengan berbagai parameter yang nantinya akan dijalankan

<br>

### **Dockerfile**
Didalam file Dockerfile akan berisi command-command yang dijalankan dalam proses penyusunan/build Docker Image. Docker Image ini kemudian akan digunakan untuk membuat dan menjalankan container baru. Isi dari Dockerfile kami sebagai berikut :

```Dockerfile
FROM ubuntu

ADD ./client /E07Sql/client

ADD ./dump /E07Sql/dump

WORKDIR /E07Sql
```

Keyword ``FROM`` digunakan untuk menyatakan base image yang digunakan. Base image tersebut bisa berupa docker image yang telah dibuat sendiri atau terdapat dalam dockerhub. Keyword ``ADD`` Akan mengcopy isi dari directory source ke diretory destination. Kemudian, keyword ``WORKDIR`` Akan mengubah working directory ketika docker container dijalankan.

<br>

### **docker-compose.yml**
docker-compose.yml akan digunakan untuk mendefinisikan container-container yang akan dibuat dan dijalankan beserta parameter tambahan yang diperlukan. Isi dari file docker-compose.yml kami sebagai berikut :

```yml
version: '3'
services:
  sukulilo:
    container_name: Sukulilo
    build: .
    image: storage-app
    tty: true
    network_mode: host
  keputih:
    container_name: Keputih
    build: .
    image: storage-app
    tty: true
    network_mode: host
  gebang:
    container_name: Gebang
    build: .
    image: storage-app
    tty: true
    network_mode: host
  mulyos:
    container_name: Mulyos
    build: .
    image: storage-app
    tty: true
    network_mode: host
  semolowaru:
    container_name: Semolowaru
    build: .
    image: storage-app
    tty: true
    network_mode: host
```

``version`` Menyatakan versi docker-compose yang digunakan. Didalam ``service`` berisi definisi masing-masing container beserta parameter masing-masing. Dalam kasus kami, memerlukan ``container_name`` untuk menentukan nama container, ``build`` menentukan lokasi Dockerfile (Ketika docker image belum disusun/build), ``image`` menentukan nama image yang akan digunakan, ``tty`` menentukan agar menjalankan terminal ketika docker container dijalankan (Agar container tidak instant close saat dijalankan), dan ``network_mode`` menentukan mode docker network yang digunakan yaitu host agar dapat menggunakan ip 127.0.0.1 yang merujuk ke host machine.

<br>

### **Cara Menggunakan**
Untuk menjalankan docker container, pertama docker image harus disusun/build terlebih dahulu. Sesuai ketentuan soal, maka command yang digunnakan adalah 
1. ``docker build -t storage-app .``. flag ``-t`` menentukan nama image yang akan di disusun/build.
2. ``docker-compose up -d`` untuk membuat dan menjalankan semua container yang terdefinisi dalam docker-compose.yml. flag ``-d`` agar command tersebut berjalan di background. 
3. ``docker ps`` untuk mengecek container yang dijalankan. 
4. ``docker exec -it [nama_container] /bin/bash`` untuk mengakses terminal container tersebut

<br>

## **Dump**

Program_dump.c digunakan untuk berinteraksi dengan server menggunakan protokol TCP/IP. Program ini memungkinkan pengguna untuk melakukan backup pada suatu database.

<br>

### **Inisialisasi Socket dan Koneksi ke Server**

<br>

```c
// Initiate Client
struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;

if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
{
    printf("\n Socket creation error \n");
    return -1;
}

memset(&serv_addr, '0', sizeof(serv_addr));

serv_addr.sin_family = AF_INET;
serv_addr.sin_port = htons(PORT);

// IP Docker : 172.28.169.226
// IP Local : 127.0.0.1
if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
{
    printf("\nInvalid address/ Address not supported \n");
    return -1;
}

if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
{
    printf("\nConnection Failed \n");
    return -1;
}
```

Pada langkah ini, kita membuat socket menggunakan fungsi `socket()` dan menginisialisasi struktur `serv_addr` yang berisi alamat IP dan port server yang akan dihubungi. Selanjutnya, kita melakukan koneksi ke server menggunakan fungsi `connect()`.

<br>

### **Autentikasi dan Input**

<br>

```c
// login
char user[STR_BUFFER];

if (getuid() == 0)
    strcpy(user, "root");
else
    strcpy(user, "user");

// Input
char database[OUT_BUFFER] = "";
char response[OUT_BUFFER] = "";
valread = read(sock, database, OUT_BUFFER);

char buffer[CMD_BUFFER];
strcpy(buffer, user);
strcat(buffer, " ");
strcat(buffer, database);
strcat(buffer, " DUMP");

for (int i = 1; i < argc; ++i){
    strcat(buffer, " ");
    strcat(buffer, argv[i]);
}
send(sock, buffer, CMD_BUFFER, 0);
valread = read(sock, response, OUT_BUFFER);
printf("%s\n", response);
```

Pada langkah ini, kita melakukan autentikasi dengan menentukan username yang akan digunakan. Jika program dijalankan dengan hak akses root, maka username akan diatur sebagai "root". Jika bukan root, maka username akan diatur sebagai "user". Kemudian perintah yang akan dikirim ke server adalah perintah `DUMP`, supaya server nantinya memproses menggunakan fungsi `handle_dump`. Kirim perintah, nama pengguna, password, dan nama database ke server. Apabila proses berhasil dijalankan pada server, maka program_dump akan menerima respon berupa *SQL script* dari database yang diminta, kemudian respon akan ditampilkan pada layar.

<br>

### **Cara Menggunakan**

1. Pastikan server sudah berjalan dan dapat diakses melalui alamat IP dan port yang sesuai.
2. Compile program dump menggunakan kompilator C seperti GCC.
3. Jalankan program dump yang telah dikompilasi.
4. Masukkan username, password, dan nama database yang sesuai untuk dikirim ke server.
5. Program akan menampilkan respons dari server.