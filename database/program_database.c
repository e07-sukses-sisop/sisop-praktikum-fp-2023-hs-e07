#include <stdio.h>
#include <time.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h> 


/* IMPORTANT VARIABLES */
#define PORT 8080
#define CMD_BUFFER 2500
#define STR_BUFFER 500
#define OUT_BUFFER 5000
#define CMD_IDX 2
#define SUB_CMD_IDX CMD_IDX + 1
#define MAX_TOKENS 100

static const char *databases_path = NULL;
char log_path[CMD_BUFFER], users_path[CMD_BUFFER];
sem_t* sem;

/* STRING UTILITIES */
void strcopy(char *str1, char *str2, size_t bufsize); // Safe strncpy
int find_strcase(char *str, char **list, size_t size); // Find str in list of str
int get_tokens(char *buffer, char **tokens, char *delimiters); // Get tokens from input string
void shift_left(char *str); // Shift string to the left

/* MORE UTILITIES */
void get_path(); // Assign value to databases_path
char *get_timestamp(); // Get current timestamp
void write_log(const char *username, const char *cmd); // Write log
void initiate_database(char *database); // Initiate root database
int check_user(char *username); // Check if user exists
int check_user_password(char *username, char *password); // Check if user exists and if password is correct
int check_arguments(int args, int num, char *output, int failed, int success); // Check number of arguments
int check_user_login(char **tokens, char *output, int num_tokens, int failed, int success, int dumpFlag); // check_arguments and check_user_passwords for "user"
int check_db(char *db_name); // Check if database exists
int check_permission(char *db_name, char *user_name); // Check user permission on a database
int check_db_permission(char *db_name, char *user_name, char *output, int failed, int success); // check_db and check_permission combined
int check_table(char *db_name, char *table_name); // Check if a table exists on a database
const char *value_type(char *value); // Check column datatype
int get_columns(char *table_path, char **columns); // Get columns and datatypes
int find_column(char **columns, char* column, size_t num_tokens); // Find column
void select_output(char* output); // Write output of select to output variable
int get_tables(char *db_name, char **tables); // Get all properties of a table

/* COMMAND HANDLE */
int handle_login(char **tokens, char *output, int num_tokens);
int handle_dump(char **tokens, char *output, int num_tokens);
int handle_read(char **tokens, char *output, int num_tokens);
int handle_create(char **tokens, char *output); // Function to handle different kinds of CREATE command
int handle_create_user(char **tokens, char *output); // Function to handle grant permission
int handle_create_database(char **tokens, char *output);
int handle_create_table(char **tokens, char *output);
int handle_use(char **tokens, char *output);
int handle_grant_permission(char **tokens, char *output); // Function to handle user creation
int handle_drop(char **tokens, char *output); // Function to handle different kinds of DROP command
int handle_drop_database(char **tokens, char *output);
int handle_drop_table(char **tokens, char *output);
int handle_drop_column(char **tokens, char *output);
int handle_insert(char **tokens, char *output, int num_tokens);
int handle_delete(char **tokens, char *output); // Function to handle delete command
int handle_update(char **tokens, char* output); // Function to handle update command
int handle_select(char **tokens, char* output); // Function to handle select command
int parse_command(char *cmd, char* output); // Parse commands

int main()
{
    // Inititae path
    get_path();

    // Inititae daemon


    // initiate server
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if(listen(server_fd, 3) < 0){
        perror("listen");
        exit(EXIT_FAILURE);
    }

    // Initiate Database
    char database[CMD_BUFFER];
    initiate_database(database);

    sem = sem_open("semaphore", O_CREAT, 0644, 2);
    sem_init(sem, 1, 1);

    pid_t child_pid;

    // Run Database
    while(1)
    {
        if((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0){
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if((child_pid = fork()) == 0) {
            close(server_fd);

            while(1)
            {
                send(new_socket, database, OUT_BUFFER, 0);

                char input_command[CMD_BUFFER] = "";
                valread = read(new_socket, input_command, CMD_BUFFER);

                sem_wait(sem);
                char response[OUT_BUFFER] = "";
                int result = parse_command(input_command, response);
                send(new_socket, response, OUT_BUFFER, 0);
                sem_post(sem);

                int isBreak = 0;
                switch(result){
                    case 2:
                        printf("Use Success\n");
                        strcopy(database, input_command, OUT_BUFFER);
                        break;
                    case 3:
                        printf("Exited\n");
                        isBreak = 1;
                        break;
                    case 4:
                        printf("Login Failed\n");
                        isBreak = 1;
                        break;
                    case 5:
                        printf("Dump Failed\n");
                        isBreak = 1;
                        break;
                    case 6:
                        printf("Dump Success\n");
                        isBreak = 1;
                        break;
                    case 7:
                        printf("Read Failed\n");
                        isBreak = 1;
                        break;
                    case 8:
                        printf("Read Success\n");
                        strcopy(database, input_command, OUT_BUFFER);
                        break;
                    case 9:
                        printf("Unexpected Error\n");
                        isBreak = 1;
                        break;
                    default:
                        break;
                }
                if(isBreak)
                    break;
            }
        }
    }
}

/* STRING UTILITIES */
void strcopy(char* str1, char* str2, size_t bufsize)
{
    strncpy(str1, str2, bufsize);
    if(str1[bufsize-1] != '\0') str1[bufsize] = '\0';
}

int find_strcase(char *str, char **list, size_t size)
{
    for(int i = 0; i < size; i++) {
        if(list[i] == NULL) break;
        if(strcasecmp(str, list[i]) == 0) return i;
    }
    return -1;
}

void shift_left(char *str)
{
    for(int i = 0; str[i] != '\0'; i++) {
        str[i] = str[i+1];
    }
}

int get_tokens(char *buffer, char **tokens, char *delimiters)
{
    int i = 0;
    char *after_quote = NULL;
    tokens[i] = strtok(buffer, delimiters);
    while(tokens[i] != NULL) {
        // Get quoted token
        if(tokens[i][0] == '\'') {
            tokens[i][strlen(tokens[i])] = ' ';
            for(int j = 1; tokens[i][j] != '\0'; j++) {
                if(tokens[i][j] == '\'') {
                    // Handle escaped quote
                    if(tokens[i][j+1] == '\'') {
                        shift_left(tokens[i] + j);
                        j++;
                        continue;
                    }
                    tokens[i][j+1] = '\0';
                    after_quote = &tokens[i][j+2];
                    break;
                }
            }
        }
        // Get next token
        i++;
        if(after_quote) {
            tokens[i] = strtok(after_quote, delimiters);
            after_quote = NULL;
        } else {
            tokens[i] = strtok(NULL, delimiters);
        }
        
    }
    return i;
}

/* MORE UTILITIES */
void get_path()
{
    system("pwd > temp.txt");
    char buffer[CMD_BUFFER];
    FILE *fp;
    fp = fopen("temp.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file temp.txt\n");
        exit(1);
    }
    fgets(buffer, CMD_BUFFER, fp);

    if(buffer[strlen(buffer) - 1] == '\n'){
        buffer[strlen(buffer) - 1] = '\0';
    }
    fclose(fp);
    system("rm temp.txt");

    // assign value to databases_path
    size_t len = strlen(buffer) + 1;
    char *temp_databases = (char *)malloc(len);
    strcpy(temp_databases, buffer);
    databases_path = temp_databases;
}

char *get_timestamp()
{
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char *timestamp = (char *) malloc(20 * sizeof(char));

    strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm_info);
    return timestamp;
}

void write_log(const char *username, const char *cmd)
{
    FILE *fp;
    fp = fopen(log_path, "a");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    char *timestamp = get_timestamp();
    fprintf(fp, "%s:%s:%s\n", timestamp, username, cmd);
    fclose(fp);
    free(timestamp);
}

void initiate_database(char *database)
{
    char cmdDB[CMD_BUFFER], cmdUser[CMD_BUFFER], cmdLog[CMD_BUFFER];

    snprintf(cmdDB, sizeof(cmdDB), "mkdir -p %s/databases", databases_path);
    snprintf(cmdUser, sizeof(cmdUser), "touch %s/users.txt", databases_path);
    snprintf(cmdLog, sizeof(cmdLog), "touch %s/log.txt", databases_path);

    snprintf(users_path, sizeof(users_path), "%s/users.txt", databases_path);
    snprintf(log_path, sizeof(log_path), "%s/log.txt", databases_path);

    system(cmdDB);
    system(cmdUser);
    system(cmdLog);
    strcopy(database, "databases", OUT_BUFFER);
}

int check_user(char *username)
{
    FILE* fp = fopen(users_path, "r");
    char buffer[STR_BUFFER];
    int found = 0;

    while(fscanf(fp, "%s", buffer) != EOF) {
        buffer[strcspn(buffer, "\n")] = 0;
        if(strcmp(buffer, username) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);

    if(!found) return 0;
    return 1;
}

int check_user_password(char *username, char *password)
{
    char buffer[STR_BUFFER];
    char users[STR_BUFFER][STR_BUFFER];
    int idx = 0, found = -1;
    FILE *fp = fopen(users_path, "r");

    if(fp != NULL){
        while(fscanf(fp, "%s", buffer) != EOF){
            buffer[strcspn(buffer, "\n")] = 0;
            strcpy(users[idx++], buffer);
        }
        for (int i = 0; i < idx; i += 2){
            if(strcmp(username, users[i]) == 0){
                found = i;
                break;
            }
        }
        if(found == -1)
            return 0;
        if(strcmp(password, users[found + 1]) != 0)
            return 1;
        fclose(fp);
        return 2;
    }
    return -1;
}

int check_arguments(int args, int num, char *output, int failed, int success)
{
    if(args < num){
        printf("Need more arguments!\n");
        strcopy(output, "Need more arguments!", OUT_BUFFER);
        return failed;
    }
    if(args > num){
        printf("Too many arguments!\n");
        strcopy(output, "Too many arguments!", OUT_BUFFER);
        return failed;
    }
    return success;
}

int check_user_login(char **tokens, char *output, int num_tokens, int failed, int success, int dumpFlag)
{
    int args = num_tokens - 3;
    int num = (dumpFlag ? 6 : 4);

    // check arguments
    if(check_arguments(args, num, output, failed, success) == failed)
        return failed;
    if(strcasecmp(tokens[SUB_CMD_IDX], "-u") != 0 || strcasecmp(tokens[SUB_CMD_IDX + 2], "-p") != 0){
        printf("Invalid arguments!\n");
        strcopy(output, "Invalid arguments!", OUT_BUFFER);
        return failed;
    }
    if(dumpFlag && strcasecmp(tokens[SUB_CMD_IDX + 4], "-d") != 0){
        printf("Invalid arguments!\n");
        strcopy(output, "Invalid arguments!", OUT_BUFFER);
        return failed;
    }

    // check user and password
    int flag = check_user_password(tokens[SUB_CMD_IDX + 1], tokens[SUB_CMD_IDX + 3]);

    switch(flag){
        case -1:
            printf("Cannot open users.txt!\n");
            strcopy(output, "Cannot open users.txt!", OUT_BUFFER);
            return failed;
        case 0:
            printf("Username not found!\n");
            strcopy(output, "Username not found!", OUT_BUFFER);
            return failed;
        case 1:
            printf("Wrong password!\n");
            strcopy(output, "Wrong password!", OUT_BUFFER);
            return failed;
        case 2:
            printf("Correct username and password!\n");
            return success;
        default:
            printf("Unknown Error!\n");
            strcopy(output, "Unknown Error!", OUT_BUFFER);
            return failed;
    }
}

int check_db(char *db_name)
{
    char cmd[CMD_BUFFER], temp_path[CMD_BUFFER];
    sprintf(temp_path, "%s/temp", databases_path);
    sprintf(cmd, "ls %s/databases > %s/temp", databases_path, databases_path);

    system(cmd);
    FILE* fp = fopen(temp_path, "r");
    char buffer[STR_BUFFER];
    int found = 0;

    while(fscanf(fp, "%s", buffer) != EOF) {
        buffer[strcspn(buffer, "\n")] = 0;
        if(strcmp(buffer, "users.txt") == 0) continue;
        if(strcmp(buffer, db_name) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);

    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);

    if(!found) return 0;
    return 1;
}

int check_permission(char *db_name, char *user_name)
{
    if(strcmp(user_name, "root") == 0){
        return 1;
    }
    char cmd[STR_BUFFER];
    char buffer[STR_BUFFER];
    sprintf(cmd, "cat %s/databases/%s/permission.txt > %s/temp", databases_path, db_name, databases_path);
    system(cmd);

    char temp_path[CMD_BUFFER];
    sprintf(temp_path, "%s/temp", databases_path);

    int found = 0;
    FILE* fp = fopen(temp_path, "r");
    while(fscanf(fp, "%s", buffer) != EOF){
        buffer[strcspn(buffer, "\n")] = 0;
        if(strcmp(buffer, user_name) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);

    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);

    if(!found) return 0;
    return 1;
}

int check_db_permission(char *db_name, char *user_name, char *output, int failed, int success)
{
    if(!check_db(db_name)){
        printf("Database not found!\n");
        strcopy(output, "Database not found!", OUT_BUFFER);
        return failed;
    }
    if(!check_permission(db_name, user_name)){
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return failed;
    }
    return success;
}

int check_table(char *db_name, char *table_name)
{
    char cmd[STR_BUFFER];
    char buffer[STR_BUFFER];
    sprintf(cmd, "ls %s/databases/%s > %s/temp;", databases_path, db_name, databases_path);
    system(cmd);
    
    char temp_path[CMD_BUFFER];
    sprintf(temp_path, "%s/temp", databases_path);

    int found = 0;
    FILE* fp = fopen(temp_path, "r");

    while(fscanf(fp, "%s", buffer) != EOF) {
        buffer[strcspn(buffer, "\n")] = 0;
        if (strcmp(buffer, "permission.txt") == 0)
            continue;
        if(strcmp(buffer, table_name) == 0) {
            found = 1;
            break;
        }
    }
    fclose(fp);

    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);

    if(!found) return 0;
    return 1;
}

const char* value_type(char *value)
{
    if(value[0] == '\'' && value[strlen(value)-1] == '\'') {
        return "string";
    }

    if(value[0] != '\'' && value[strlen(value)-1] != '\'') {
        if(strcmp(value, "0") != 0 && atoi(value) == 0) {
            return "unknown";
        }
        return "int";
    }

    return "unknown";
}

int get_columns(char *table_path, char **columns)
{
    char buffer[STR_BUFFER];
    FILE* fp = fopen(table_path, "r");
    fgets(buffer, STR_BUFFER, fp);
    int num_tokens = get_tokens(buffer, columns, " :\n");
    for(int i = 0; i < num_tokens; i++) {
        printf("colums and data : %s\n", columns[i]);
    }
    fclose(fp);
    return num_tokens;
}

int find_column(char **columns, char* column, size_t num_tokens)
{
    int collnum = find_strcase(column, columns, num_tokens) + 1;
    if(collnum == 0 || collnum % 2 == 0) return 0;
    return collnum;
}

void select_output(char *output)
{
    char temp_path[CMD_BUFFER];
    sprintf(temp_path, "%s/temp", databases_path);

    FILE *fp = fopen(temp_path, "r");
    char temp[STR_BUFFER];

    while(fgets(temp, STR_BUFFER, fp) != NULL){
        strcat(output, temp);
    }

    *strrchr(output, '\n') = '\0';
    fclose(fp);
}

int get_tables(char *db_name, char **tables)
{
    char cmd[STR_BUFFER];
    char buffer[STR_BUFFER];
    sprintf(cmd, "ls %s/databases/%s > %s/temp;", databases_path, db_name, databases_path);
    system(cmd);

    char temp_path[CMD_BUFFER];
    sprintf(temp_path, "%s/temp", databases_path);
    sprintf(cmd, "touch %s/temp", databases_path);

    int found = 0, idx = 0;
    system(cmd);
    FILE *fp = fopen(temp_path, "r");

    if(fp != NULL){
        while(fscanf(fp, "%s", buffer) != EOF) {
            buffer[strcspn(buffer, "\n")] = 0;
            if (strcmp(buffer, "permission.txt") == 0)
                continue;

            tables[idx] = (char *)malloc(STR_BUFFER);
            strcopy(tables[idx], buffer, STR_BUFFER);
            idx++;
        }
        fclose(fp);
    }
    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);
    return idx;
}

/* COMMAND HANDLE */
int handle_login(char **tokens, char *output, int num_tokens)
{
    int args = num_tokens - 3;
    // 4 = FAILED
    // 1 = SUCCESS

    // root
    if(strcmp(tokens[0], "root") == 0){
        if(check_arguments(args, 0, output, 4, 1) == 4)
            return 4;
    }
    // user
    else {
        if(check_user_login(tokens, output, num_tokens, 4, 1, 0) == 4)
            return 4;
    }
    printf("Login Success!\n");
    strcopy(output, "Login Success!", OUT_BUFFER);
    return 1;
}

int handle_dump(char **tokens, char *output, int num_tokens)
{
    char database[STR_BUFFER];
    int args = num_tokens - 3;
    // 5 = FAILED
    // 6 = SUCCESS

    // root
    if(strcmp(tokens[0], "root") == 0){
        // check arguments
        if(check_arguments(args, 2, output, 5, 6) == 5)
            return 5;
        if(strcasecmp(tokens[SUB_CMD_IDX], "-d") != 0){
            printf("Invalid arguments!\n");
            strcopy(output, "Invalid arguments!", OUT_BUFFER);
            return 5;
        }

        // check db
        strcopy(database, tokens[SUB_CMD_IDX + 1], STR_BUFFER);
        if(!check_db(database)){
            printf("Database not found!\n");
            strcopy(output, "Database not found!", OUT_BUFFER);
            return 5;
        }
    }
    // user
    else {
        // check login
        if(check_user_login(tokens, output, num_tokens, 5, 6, 1) == 5)
            return 5;

        // check db and permission
        strcopy(database, tokens[SUB_CMD_IDX + 5], STR_BUFFER);
        if(check_db_permission(database, tokens[SUB_CMD_IDX + 1], output, 5, 6) == 5)
            return 5;
    }
    printf("All checking success!\n");

    // Get all tables
    char *tables[STR_BUFFER];
    int num_tables = get_tables(database, (char **)tables);

    printf("Database %s has total of %d table(s)\n", database, num_tables);
    strcopy(output, "", OUT_BUFFER);

    for(int i = 0; i < num_tables; ++i)
    {
        char *columns[MAX_TOKENS];
        char table_path[CMD_BUFFER], buffer[STR_BUFFER];
        sprintf(table_path, "%s/databases/%s/%s", databases_path, database, tables[i]);

        FILE *fp = fopen(table_path, "r");
        if(fp == NULL){
            printf("Failed to open table!\n");
            strcopy(output, "Failed to open table!", OUT_BUFFER);
            return 5;
        }
        char temp[STR_BUFFER];
        fgets(temp, STR_BUFFER, fp);

        int num_columns = get_tokens(temp, columns, " :\n");

        sprintf(buffer, "DROP TABLE %s;\n", tables[i]);
        strcat(output, buffer);
        sprintf(buffer, "CREATE TABLE %s (", tables[i]);
        strcat(output, buffer);

        for(int j = 0; j < num_columns; ++j){
            if(j > 0)
                strcat(output, " ");
            sprintf(buffer, "%s", columns[j]);
            strcat(output, buffer);

            if(j % 2 == 1 && j != num_columns - 1)
                strcat(output, ",");
        }
        strcat(output, ");\n");
        num_columns >>= 1;

        fclose(fp);
        fp = fopen(table_path, "r");
        int row = 0;

        if(fp == NULL){
            printf("Failed to open table!\n");
            strcopy(output, "Failed to open table!", OUT_BUFFER);
            return 5;
        }
        char line[STR_BUFFER];

        while(fgets(line, sizeof(line), fp) != NULL)
        {
            if(row > 0){
                line[strcspn(line, "\n")] = 0;

                char buffer2[STR_BUFFER];
                strcopy(buffer2, line, STR_BUFFER);

                char *tokenss[MAX_TOKENS];
                int num_tokenss = get_tokens(buffer2, tokenss, " \n");

                sprintf(buffer, "INSERT INTO %s (", tables[i]);
                strcat(output, buffer);

                for(int j = 0; j < num_columns; ++j){
                    if(j > 0)
                        strcat(output, " ");
                    sprintf(buffer, "%s", tokenss[j]);
                    strcat(output, buffer);

                    if(j != num_columns - 1)
                        strcat(output, ",");
                }
                strcat(output, ");\n");
            }
            row++;
        }
        fclose(fp);
        free(tables[i]);
        strcat(output, "\n");
    }
    printf("Backup Success!\n");
    return 6;
}

int handle_read(char **tokens, char *output, int num_tokens)
{
    char database[STR_BUFFER];
    int args = num_tokens - 3;
    // 7 = FAILED
    // 8 = SUCCESS

    // root
    if(strcmp(tokens[0], "root") == 0){
        // check arguments
        if(check_arguments(args, 2, output, 7, 8) == 7)
            return 7;
        if(strcasecmp(tokens[SUB_CMD_IDX], "-d") != 0){
            printf("Invalid arguments!\n");
            strcopy(output, "Invalid arguments!", OUT_BUFFER);
            return 7;
        }

        // check db
        strcopy(database, tokens[SUB_CMD_IDX + 1], STR_BUFFER);
        if(!check_db(database)){
            printf("Database not found!\n");
            strcopy(output, "Database not found!", OUT_BUFFER);
            return 7;
        }
    }
    // user
    else {
        // check login
        if(check_user_login(tokens, output, num_tokens, 7, 8, 1) == 7)
            return 7;

        // check db and permission
        strcopy(database, tokens[SUB_CMD_IDX + 5], STR_BUFFER);
        if(check_db_permission(database, tokens[SUB_CMD_IDX + 1], output, 7, 8) == 7)
            return 7;
    }
    printf("All checking success!\n");
    strcopy(output, "All checking success!", OUT_BUFFER);
    return 8;
}

int handle_create(char **tokens, char *output)
{
    if(tokens[SUB_CMD_IDX] == NULL) {
        printf("Expected USER|DATABASE|TABLE\n");
        strcopy(output, "Expected USER|DATABASE|TABLE", OUT_BUFFER);
        return 0;
    }
    int result = 0;

    char *sublist[] = {"USER", "DATABASE", "TABLE"};
    switch(find_strcase(tokens[SUB_CMD_IDX], sublist, 3)) {
        case 0:
            printf("CREATE USER command\n");
            result = handle_create_user(tokens, output);
            break;
        case 1:
            printf("CREATE DATABASE command\n");
            result = handle_create_database(tokens, output);
            break;
        case 2:
            printf("CREATE TABLE command\n");
            result = handle_create_table(tokens, output);
            break;
        default:
            printf("Expected USER|DATABASE|TABLE\n");
            strcopy(output, "Expected USER|DATABASE|TABLE", OUT_BUFFER);
            result = 0;
            break;
        }
    return result;
}

int handle_create_user(char **tokens, char* output)
{
    if(strcmp(tokens[0], "root") != 0) {
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected username!\n");
        strcopy(output, "Expected username!", OUT_BUFFER);
        return 0;
    }

    if(check_user(tokens[SUB_CMD_IDX + 1])) {
        printf("User exists!\n");
        return 0;
    }
    
    if(tokens[SUB_CMD_IDX + 2] == NULL || strcasecmp(tokens[SUB_CMD_IDX + 2], "IDENTIFIED") != 0 ||\
       tokens[SUB_CMD_IDX + 3] == NULL || strcasecmp(tokens[SUB_CMD_IDX + 3], "BY") != 0 || tokens[SUB_CMD_IDX + 4] == NULL) {
        printf("Invalid syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    char cmd[CMD_BUFFER];
    sprintf(cmd, "echo '%s %s' >> %s/users.txt", tokens[SUB_CMD_IDX + 1], tokens[SUB_CMD_IDX + 4], databases_path);
    system(cmd);
    printf("Added user!\n");
    strcopy(output, "Added user!", OUT_BUFFER);
    return 1;
}

int handle_create_database(char **tokens, char *output)
{
    // no database name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Database Name!\n");
        strcopy(output, "Expected Database Name!", OUT_BUFFER);
        return 0;
    }

    // check db already created
    if(check_db(tokens[SUB_CMD_IDX + 1])){
        printf("Database already created\n");
        strcopy(output, "Database already created!", OUT_BUFFER);
        return 0;
    }

    // create database
    char cmdDB[CMD_BUFFER], cmdPerm[CMD_BUFFER];
    snprintf(cmdDB, sizeof(cmdDB), "mkdir %s/databases/%s", databases_path, tokens[SUB_CMD_IDX + 1]);
    system(cmdDB);

    // create permission
    snprintf(cmdPerm, sizeof(cmdPerm), "touch %s/databases/%s/permission.txt", databases_path, tokens[SUB_CMD_IDX + 1]);
    system(cmdPerm);

    if(strcmp(tokens[0], "root") != 0)
    {
        char permPath[CMD_BUFFER];
        snprintf(permPath, sizeof(permPath), "%s/databases/%s/permission.txt", databases_path, tokens[SUB_CMD_IDX + 1]);

        FILE *fp = fopen(permPath, "w");
        if(fp == NULL){
            printf("Failed to open %s\n", permPath);
            strcopy(output, "Failed to open ", OUT_BUFFER);
            strcat(output, permPath);
            return 0;
        }
        fprintf(fp, "%s\n", tokens[0]);
        fclose(fp);
    }
    printf("Added database!\n");
    strcopy(output, "Added database!", OUT_BUFFER);
    return 1;
}

int handle_create_table(char **tokens, char *output)
{
    // no table name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    // check db and user permission
    if(check_db_permission(tokens[1], tokens[0], output, 0, 1) == 0)
        return 0;

    // check table already created
    if(check_table(tokens[1], tokens[SUB_CMD_IDX + 1])){
        printf("Table already created\n");
        strcopy(output, "Table already created!", OUT_BUFFER);
        return 0;
    }

    // create table
    char tablePath[CMD_BUFFER], cmdTable[CMD_BUFFER];
    snprintf(tablePath, sizeof(tablePath), "%s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 1]);
    snprintf(cmdTable, sizeof(cmdTable), "touch %s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 1]);
    system(cmdTable);

    FILE *fp = fopen(tablePath, "w");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    char buffer[CMD_BUFFER] = "";

    for(int i = SUB_CMD_IDX + 2; tokens[i] != NULL; i += 2)
    {
        if(i > SUB_CMD_IDX + 2){
            strcat(buffer, " ");
        }
        strcat(buffer, tokens[i]);
        strcat(buffer, ":");
        strcat(buffer, tokens[i + 1]);
    }
    fprintf(fp, "%s\n", buffer);
    fclose(fp);

    printf("Added table!\n");
    strcopy(output, "Added table!", OUT_BUFFER);
    return 1;
}

int handle_use(char **tokens, char *output)
{
    // no database name
    if(tokens[SUB_CMD_IDX] == NULL){
        printf("Expected Database Name!\n");
        strcopy(output, "Expected Database Name!", OUT_BUFFER);
        return 0;
    }

    //check db and user permission
    if(check_db_permission(tokens[SUB_CMD_IDX], tokens[0], output, 0, 2) == 0)
        return 0;
    
    // success
    strcopy(output, "Success!", OUT_BUFFER);
    return 2;
}

int handle_grant_permission(char **tokens, char* output)
{
    if(strcmp(tokens[0], "root") != 0) {
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    if(tokens[SUB_CMD_IDX] == NULL || strcasecmp(tokens[SUB_CMD_IDX], "PERMISSION") != 0) {
        printf("Invalid syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected database name!\n");
        strcopy(output, "Expected database name!", OUT_BUFFER);
        return 0;
    }

    if(!check_db(tokens[SUB_CMD_IDX + 1])) {
        printf("Database not found!\n");
        strcopy(output, "Database not found!", OUT_BUFFER);
        return 0;
    }

    if(tokens[SUB_CMD_IDX + 2] == NULL || strcasecmp(tokens[SUB_CMD_IDX + 2], "INTO") != 0 || tokens[SUB_CMD_IDX + 3] == NULL) {
        printf("Invalid syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    if(!check_user(tokens[SUB_CMD_IDX + 3])) {
        printf("Invalid user!\n");
        strcopy(output, "Invalid user!", OUT_BUFFER);
        return 0;
    }

    if(check_permission(tokens[SUB_CMD_IDX + 1], tokens[SUB_CMD_IDX + 3])) {
        printf("User already has permission for this database!\n");
        strcopy(output, "User already has permission for this database!", OUT_BUFFER);
        return 0;
    }

    char cmd[CMD_BUFFER];
    sprintf(cmd,"echo '%s' >> %s/databases/%s/permission.txt", tokens[SUB_CMD_IDX + 3], databases_path, tokens[SUB_CMD_IDX + 1]);
    system(cmd);
    printf("Added permission for user!\n");
    strcopy(output, "Added permission for user!", OUT_BUFFER);
    return 1;
}

int handle_drop(char **tokens, char *output)
{
    if(tokens[SUB_CMD_IDX] == NULL) {
        printf("Expected DATABASE|TABLE|COLUMN\n");
        strcopy(output, "Expected DATABASE|TABLE|COLUMN", OUT_BUFFER);
        return 0;
    }
    int result = 0;

    char *sublist[] = {"DATABASE", "TABLE", "COLUMN"};
    switch(find_strcase(tokens[SUB_CMD_IDX], sublist, 3)) {
        case 0:
            printf("DROP DATABASE command\n");
            result = handle_drop_database(tokens, output);
            break;
        case 1:
            printf("DROP TABLE command\n");
            result = handle_drop_table(tokens, output);
            break;
        case 2:
            printf("DROP COLUMN command\n");
            result = handle_drop_column(tokens, output);
            break;
        default:
            printf("Expected DATABASE|TABLE|COLUMN\n");
            strcopy(output, "Expected DATABASE|TABLE|COLUMN", OUT_BUFFER);
            result = 0;
            break;
        }
    return result;
}

int handle_drop_database(char **tokens, char *output)
{
    // no database name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Database Name!\n");
        strcopy(output, "Expected Database Name!", OUT_BUFFER);
        return 0;
    }

    //check db and user permission
    if(check_db_permission(tokens[SUB_CMD_IDX + 1], tokens[0], output, 0, 1) == 0)
        return 0;

    // drop database
    char cmdDB[CMD_BUFFER];
    snprintf(cmdDB, sizeof(cmdDB), "rm -r %s/databases/%s", databases_path, tokens[SUB_CMD_IDX + 1]);
    system(cmdDB);

    printf("Removed database!\n");
    strcopy(output, "Removed database!", OUT_BUFFER);
    return 1;
}

int handle_drop_table(char **tokens, char *output)
{
    // no table name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    //check db and user permission
    if(check_db_permission(tokens[1], tokens[0], output, 0, 1) == 0)
        return 0;

    // check table not found
    if(!check_table(tokens[1], tokens[SUB_CMD_IDX + 1])){
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    // drop table
    char cmdTable[CMD_BUFFER];
    snprintf(cmdTable, sizeof(cmdTable), "rm %s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 1]);
    system(cmdTable);

    printf("Removed table!\n");
    strcopy(output, "Removed table!", OUT_BUFFER);
    return 1;
}

int handle_drop_column(char **tokens, char *output)
{
    // no column name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Column Name!\n");
        strcopy(output, "Expected Column Name!", OUT_BUFFER);
        return 0;
    }
    
    // invalid syntax
    if(tokens[SUB_CMD_IDX + 2] == NULL){
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }
    if(strcasecmp(tokens[SUB_CMD_IDX + 2], "FROM") != 0){
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    // no table name
    if(tokens[SUB_CMD_IDX + 3] == NULL){
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    // check db and user permission
    if(!check_db(tokens[1])){
        printf("Database not found!\n");
        strcopy(output, "Database not found!", OUT_BUFFER);
        return 0;
    }
    if(!check_permission(tokens[1], tokens[0])){
        printf("Permission denied!\n");
        strcopy(output, "Permission denied!", OUT_BUFFER);
        return 0;
    }

    // check table not found
    if(!check_table(tokens[1], tokens[SUB_CMD_IDX + 3])){
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    // table path
    char tablePath[STR_BUFFER];
    snprintf(tablePath, sizeof(tablePath), "%s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 3]);

    // target column
    char column[STR_BUFFER];
    strcopy(column, tokens[SUB_CMD_IDX + 1], STR_BUFFER);

    // Get columns and datatype for said columns
    char *columns[MAX_TOKENS];
    int num_columns = get_columns(tablePath, columns);

    // Get column number
    int collnum = find_column(columns, column, num_columns);
    if(collnum == 0){
        printf("Column not found!\n");
        strcopy(output, "Column not found!", OUT_BUFFER);
        return 0;
    }
    collnum >>= 1;

    FILE *fp = fopen(tablePath, "r");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    char line[STR_BUFFER];
    char str_result[CMD_BUFFER] = "";

    while(fgets(line, sizeof(line), fp) != NULL)
    {
        line[strcspn(line, "\n")] = 0;

        char buffer[STR_BUFFER];
        strcopy(buffer, line, STR_BUFFER);

        char *tokenss[256];
        int num_tokenss = get_tokens(buffer, tokenss, " \n");

        for(int i = 0; i < num_tokenss; ++i){
            if(i == collnum)
                continue;
            else {
                if(i > 0){
                    if(!(i == 1 && collnum == 0))
                        strcat(str_result, " ");
                }
                strcat(str_result, tokenss[i]);
            }
        }
        strcat(str_result, "\n");
    }
    fclose(fp);

    fp = fopen(tablePath, "w");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    fprintf(fp, "%s", str_result);
    fclose(fp);

    printf("Removed column!\n");
    strcopy(output, "Removed column!", OUT_BUFFER);
    return 1;
}

int handle_insert(char ** tokens, char *output, int num_tokens)
{
    // no table name
    if(tokens[SUB_CMD_IDX + 1] == NULL){
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    // check db and user permission
    if(check_db_permission(tokens[1], tokens[0], output, 0, 1) == 0)
        return 0;

    // check table not found
    if(!check_table(tokens[1], tokens[SUB_CMD_IDX + 1])){
        printf("Table not found\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }
    
    // insert
    char *columns[MAX_TOKENS];
    char tablePath[CMD_BUFFER];
    snprintf(tablePath, sizeof(tablePath), "%s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 1]);

    FILE *fp = fopen(tablePath, "r");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    char temp[STR_BUFFER];
    fgets(temp, STR_BUFFER, fp);

    int num_columns = get_tokens(temp, columns, " :\n");
    num_columns >>= 1;

    fclose(fp);
    fp = fopen(tablePath, "a");
    if(fp == NULL){
        printf("Failed to open %s\n", tablePath);
        strcopy(output, "Failed to open ", OUT_BUFFER);
        strcat(output, tablePath);
        return 0;
    }
    char buffer[CMD_BUFFER] = "";

    int col = 0;
    for(int i = SUB_CMD_IDX + 2; tokens[i] != NULL; ++i){
        col++;
    }
    if(col != num_columns) {
        printf("Number of values inserted does not match with number of columns!\n");
        strcopy(output, "Number of values inserted does not match with number of columns!", OUT_BUFFER);
        return 0;
    }
    for(int i = SUB_CMD_IDX + 2; tokens[i] != NULL; ++i)
    {
        if(i > SUB_CMD_IDX + 2){
            strcat(buffer, " ");
        }
        strcat(buffer, tokens[i]);
    }
    fprintf(fp, "%s\n", buffer);
    fclose(fp);

    printf("Added values!\n");
    strcopy(output, "Added values!", OUT_BUFFER);
    return 1;
}

int handle_delete(char **tokens, char* output)
{
    if(tokens[SUB_CMD_IDX] == NULL) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    if(strcasecmp(tokens[SUB_CMD_IDX], "FROM") != 0) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    // check db and user permission
    if(check_db_permission(tokens[1], tokens[0], output, 0, 1) == 0)
        return 0;

    if(tokens[SUB_CMD_IDX + 1] == NULL) {
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    if(!check_table(tokens[1], tokens[SUB_CMD_IDX + 1])) {
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    char table_path[STR_BUFFER];
    sprintf(table_path, "%s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX + 1]);


    // Delete all rows
    if(tokens[SUB_CMD_IDX + 2] == NULL) {
        char cmd[CMD_BUFFER];
        sprintf(cmd, "awk \"NR==1 {print\\$0}\" %s > %s/temp", table_path, databases_path);
        system(cmd);
        sprintf(cmd, "cat %s/temp > %s", databases_path, table_path);
        system(cmd);
        sprintf(cmd, "rm %s/temp", databases_path);
        system(cmd);
        strcopy(output, "Delete table success!", OUT_BUFFER);
        return 1;
    }

    if(strcasecmp(tokens[SUB_CMD_IDX + 2], "WHERE") != 0 || tokens[SUB_CMD_IDX + 3] == NULL || tokens[SUB_CMD_IDX + 4] == NULL) {
        printf("Invalid syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    char column[STR_BUFFER], value[STR_BUFFER];
    strcopy(column, tokens[SUB_CMD_IDX + 3], STR_BUFFER);
    strcopy(value, tokens[SUB_CMD_IDX + 4], STR_BUFFER);

    // Get columns and datatype for said columns
    char *columns[MAX_TOKENS];
    int num_tokens = get_columns(table_path, columns);

    // Get column number
    int collnum = find_column(columns, column, num_tokens);
    if(collnum == 0) {
        printf("Column not found!\n");
        strcopy(output, "Column not found!", OUT_BUFFER);
        return 0;
    }

    // Check data type of collumn and input
    if(strcmp(columns[collnum], value_type(value)) != 0) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    // Delete matching rows
    char cmd[CMD_BUFFER];
    sprintf(cmd, "awk \" \\$%d!=\\\"%s\\\"{print \\$0} \" %s > %s/temp", (collnum + 1) / 2, value, table_path, databases_path);
    system(cmd);
    sprintf(cmd, "cat %s/temp > %s", databases_path, table_path);
    system(cmd);
    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);
    strcopy(output, "Success delete rows!", OUT_BUFFER);
    return 1;
}

int handle_update(char **tokens, char* output)
{
    if(tokens[SUB_CMD_IDX + 1] == NULL || strcasecmp(tokens[SUB_CMD_IDX + 1], "SET") != 0 || tokens[SUB_CMD_IDX + 2] == NULL || tokens[SUB_CMD_IDX + 3] == NULL) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    //check db and user permission
    if(check_db_permission(tokens[1], tokens[0], output, 0, 1) == 0)
        return 0;

    if(tokens[SUB_CMD_IDX] == NULL) {
        printf("Expected Table Name!\n");
        strcopy(output, "Expected Table Name!", OUT_BUFFER);
        return 0;
    }

    if(!check_table(tokens[1], tokens[SUB_CMD_IDX])) {
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    char table_path[STR_BUFFER];
    sprintf(table_path, "%s/databases/%s/%s", databases_path, tokens[1], tokens[SUB_CMD_IDX]);

    // Get columns and datatype for said columns

    char *columns[MAX_TOKENS];
    int num_tokens = get_columns(table_path, columns);

    char column[STR_BUFFER], value[STR_BUFFER];
    strcopy(column, tokens[SUB_CMD_IDX + 2], STR_BUFFER);
    strcopy(value, tokens[SUB_CMD_IDX + 3], STR_BUFFER);

    // Get column number
    int collnum = find_column(columns, column, num_tokens);
    if(collnum == 0) {
        printf("Column not found!\n");
        strcopy(output, "Column not found!", OUT_BUFFER);
        return 0;
    }

    // Check data type of collumn and input
    if(strcmp(columns[collnum], value_type(value)) != 0) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    // Update all rows
    if(tokens[SUB_CMD_IDX + 4] == NULL) {
        char cmd[CMD_BUFFER];
        sprintf(cmd, "awk \" BEGIN{FPAT = \\\"([^ ]+)|('[^']+')\\\"}  NR>1{gsub(\\$%d, \\\"%s\\\", \\$%d)}  {print \\$0} \" %s > %s/temp", (collnum + 1)/2, value, (collnum + 1)/2, table_path, databases_path);
        system(cmd);
        sprintf(cmd, "cat %s/temp > %s", databases_path, table_path);
        system(cmd);
        sprintf(cmd, "rm %s/temp", databases_path);
        system(cmd);
        strcopy(output, "Update table success!", OUT_BUFFER);
        return 1;
    }

    if(strcasecmp(tokens[SUB_CMD_IDX + 4], "WHERE") != 0 || tokens[SUB_CMD_IDX + 5] == NULL || tokens[SUB_CMD_IDX + 6] == NULL) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    char target_column[STR_BUFFER], target_value[STR_BUFFER];
    strcopy(target_column, tokens[SUB_CMD_IDX + 5], STR_BUFFER);
    strcopy(target_value, tokens[SUB_CMD_IDX + 6], STR_BUFFER);

    // Get column number
    int target_collnum = find_column(columns, target_column, num_tokens);
    if(target_collnum == 0) {
        printf("Column not found!\n");
        strcopy(output, "Column not found!", OUT_BUFFER);
        return 0;
    }

    // Check data type of collumn and input
    if(strcmp(columns[target_collnum], value_type(target_value)) != 0) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    // Update matching rows
    char cmd[CMD_BUFFER];
    sprintf(cmd, "awk \" BEGIN{FPAT = \\\"([^ ]+)|('[^']+')\\\"}  (NR>1 && \\$%d==\\\"%s\\\"){gsub(\\$%d, \\\"%s\\\", \\$%d)}  {print \\$0} \" %s > %s/temp",\
            (target_collnum + 1)/2, target_value, (collnum + 1)/2, value, (collnum + 1)/2, table_path, databases_path);
    system(cmd);
    sprintf(cmd, "cat %s/temp > %s", databases_path, table_path);
    system(cmd);
    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);
    strcopy(output, "Update table success!", OUT_BUFFER);
    return 1;
}

int handle_select(char **tokens, char* output)
{
    //check db and user permission
    if(check_db_permission(tokens[1], tokens[0], output, 0, 1) == 0)
        return 0;
        
    // Find FROM keyword
    int from_idx = find_strcase("FROM", tokens, MAX_TOKENS);
    if(from_idx == -1) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    if(tokens[from_idx + 1] == NULL) {
        printf("Expected table name!\n");
        strcopy(output, "Expected table name!", OUT_BUFFER);
        return 0;
    }

    if(!check_table(tokens[1], tokens[from_idx + 1])) {
        printf("Table not found!\n");
        strcopy(output, "Table not found!", OUT_BUFFER);
        return 0;
    }

    if(from_idx == CMD_IDX + 1) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    char table_path[STR_BUFFER];
    sprintf(table_path, "%s/databases/%s/%s", databases_path, tokens[1], tokens[from_idx + 1]);
    
    // Get columns and datatype for said columns
    char *columns[MAX_TOKENS];
    int num_tokens = get_columns(table_path, columns);

    // Get column numbers
    int collnums[MAX_TOKENS/2];
    memset(collnums, 0, sizeof(collnums)/sizeof(int));

    int num_columns = 0;
    // Either all columns or specific columns
    if(from_idx == SUB_CMD_IDX + 1 && strcmp(tokens[from_idx - 1], "*") == 0) {
        num_columns = 1;
    } else {
        for(int i = 3, j = 0; i < from_idx; i++) {
            printf("%s\n", tokens[i]);
            collnums[j] = (find_column(columns, tokens[i], num_tokens) + 1) / 2;
            if(collnums[j] == 0) {
                printf("Invalid column\n");
                strcopy(output, "Invalid column", OUT_BUFFER);
                return 0;
            }
            printf("colnum : %d\n", collnums[j]);
            j++;
            num_columns = j;
        }
    }
    

    if(tokens[from_idx + 2] == NULL) {
        char cmd[CMD_BUFFER];
        char columns_idx[STR_BUFFER] = "";
        char temp[STR_BUFFER];

        // Print fields
        for(int i = 0; i < num_columns; i++) {
            sprintf(temp, "\\$%d,", collnums[i]);
            strcat(columns_idx, temp);
        }
        columns_idx[strlen(columns_idx)-1] = '\0';

        printf("%s\n", columns_idx);
        sprintf(cmd, "awk \" BEGIN{FPAT = \\\"([^ ]+)|('[^']+')\\\"} {print %s} \" %s > %s/temp",\
            columns_idx, table_path, databases_path);
        system(cmd);
        select_output(output);
        printf("%s\n", output);
        sprintf(cmd, "rm %s/temp", databases_path);
        system(cmd);

        return 1;
    }

    if(strcasecmp(tokens[from_idx + 2], "WHERE") != 0 || tokens[from_idx + 3] == NULL || tokens[from_idx + 4] == NULL) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    char column[STR_BUFFER], value[STR_BUFFER];
    strcopy(column, tokens[from_idx + 3], STR_BUFFER);
    strcopy(value, tokens[from_idx + 4], STR_BUFFER);

    // Get column number
    int collnum = find_column(columns, column, num_tokens);
    if(collnum == 0) {
        printf("Column not found!\n");
        strcopy(output, "Column not found!", OUT_BUFFER);
        return 0;
    }

    // Check data type of collumn and input
    if(strcmp(columns[collnum], value_type(value)) != 0) {
        printf("Invalid Syntax!\n");
        strcopy(output, "Invalid syntax!", OUT_BUFFER);
        return 0;
    }

    char cmd[CMD_BUFFER];
    char columns_idx[STR_BUFFER] = "";
    char temp[STR_BUFFER];

    // Print fields
    for(int i = 0; i < num_columns; i++) {
        sprintf(temp, "\\$%d,", collnums[i]);
        strcat(columns_idx, temp);
    }
    columns_idx[strlen(columns_idx)-1] = '\0';

    printf("%s\n", columns_idx);
    sprintf(cmd, "awk \" BEGIN{FPAT = \\\"([^ ]+)|('[^']+')\\\"}  NR==1{print %s} (NR>1 && \\$%d==\\\"%s\\\"){print %s} \" %s > %s/temp",\
        columns_idx, (collnum+1)/2, value, columns_idx, table_path, databases_path);
    system(cmd);
    select_output(output);
    printf("%s\n", output);
    sprintf(cmd, "rm %s/temp", databases_path);
    system(cmd);
    return 1;
}

int parse_command(char *cmd, char* output)
{
    char *tokens[256];
    char buffer[CMD_BUFFER];

    strcopy(buffer, cmd, CMD_BUFFER);
    int num_tokens = get_tokens(buffer, tokens, " (),;=\n");

    // debug command
    for(int i = 0; i < num_tokens; i++) {
        printf("--> %s\n", tokens[i]);
    }

    // no username
    if(tokens[0] == NULL) {
        printf("User not received!\n");
        return 9;
    }
    // no database
    if(tokens[1] == NULL){
        printf("Database not received!\n");
        return 9;
    }
    // no command
    if(tokens[CMD_IDX] == NULL) {
        printf("Command not received!\n");
        return 9;
    }

    // Write log
    int start_cmd = strlen(tokens[0]) + strlen(tokens[1]) + 2;
    write_log(tokens[0], cmd + start_cmd);

    int result = 0;

    char *sublist[] = {"USE", "CREATE", "GRANT", "DROP", "INSERT", "UPDATE", "DELETE", "SELECT", "EXIT", "LOGIN", "DUMP", "READ"};
    switch(find_strcase(tokens[CMD_IDX], sublist, 12)) {
        case 0:
            printf("USE command\n");
            // Handle use
            // result = 0 (failed)
            // result = 2 (success)
            result = handle_use(tokens, output);
            if(result == 2)
                strcopy(cmd, tokens[SUB_CMD_IDX], CMD_BUFFER);
            break;
        case 1:
            printf("CREATE command\n");
            // Handle create
            result = handle_create(tokens, output);
            break;
        case 2:
            printf("GRANT command\n");
            // Handle grant
            result = handle_grant_permission(tokens, output);
            break;
        case 3:
            printf("DROP command\n");
            // Handle drop
            result = handle_drop(tokens, output);
            break;
        case 4:
            printf("INSERT command\n");
            // Handle insert
            result = handle_insert(tokens, output, num_tokens);
            break;
        case 5:
            printf("UPDATE command\n");
            // Handle update
            result = handle_update(tokens, output);
            break;
        case 6:
            printf("DELETE command\n");
            // Handle delete
            result = handle_delete(tokens, output);
            break;
        case 7:
            printf("SELECT command\n");
            // Handle select
            result = handle_select(tokens, output);
            break;
        case 8:
            printf("EXIT command\n");
            result = 3;
            break;
        case 9:
            printf("LOGIN command\n");
            // Handle login
            result = handle_login(tokens, output, num_tokens);
            break;
        case 10:
            printf("DUMP command\n");
            // Handle dump
            result = handle_dump(tokens, output, num_tokens);
            break;
        case 11:
            printf("READ command\n");
            // Handle read
            // result = 7 (failed)
            // result = 8 (success)
            result = handle_read(tokens, output, num_tokens);
            if(result == 8){
                if(strcmp(tokens[0], "root") == 0)
                    strcopy(cmd, tokens[SUB_CMD_IDX + 1], CMD_BUFFER);
                else
                    strcopy(cmd, tokens[SUB_CMD_IDX + 5], CMD_BUFFER);
            }
            break;
        default:
            printf("Invalid Syntax!\n");
            strcopy(output, "Invalid Syntax!", OUT_BUFFER);
            result = 0;
            break;
    }
    return result;
}
