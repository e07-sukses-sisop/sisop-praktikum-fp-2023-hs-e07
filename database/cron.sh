#!/bin/bash

# Get current path
cur_path=$(pwd)
cd $cur_path

# Get databases path
cd databases
databases_path=$(pwd)

# Copy log
cd ..
cp log.txt ../dump

# Read all databases and backup
cd ../dump
gcc program_dump.c -o program_dump

for folder in "$databases_path"/*/
do
    db=$(basename "$folder")
    sudo ./program_dump -d $db > "$db.backup"
done

# zip all backup + log
current_date=$(date +%Y-%m-%d-%H:%M:%S)
zip -q "${current_date}.zip" log.txt *.backup

# Remove all backup + log
rm log.txt *.backup

# Reset log
echo -n > ../database/log.txt
