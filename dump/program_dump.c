#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080
#define CMD_BUFFER 2500
#define STR_BUFFER 500
#define OUT_BUFFER 5000

int main(int argc, char const *argv[])
{
    // Initiate Client
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // IP Docker : 172.28.169.226
    // IP Local : 127.0.0.1
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    // login
    char user[STR_BUFFER];

    if (getuid() == 0)
        strcpy(user, "root");
    else
        strcpy(user, "user");

    // Input
    char database[OUT_BUFFER] = "";
    char response[OUT_BUFFER] = "";
    valread = read(sock, database, OUT_BUFFER);

    char buffer[CMD_BUFFER];
    strcpy(buffer, user);
    strcat(buffer, " ");
    strcat(buffer, database);
    strcat(buffer, " DUMP");

    for (int i = 1; i < argc; ++i){
        strcat(buffer, " ");
        strcat(buffer, argv[i]);
    }
    send(sock, buffer, CMD_BUFFER, 0);
    valread = read(sock, response, OUT_BUFFER);
    printf("%s\n", response);
}
