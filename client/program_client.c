#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080
#define CMD_BUFFER 2500
#define STR_BUFFER 500
#define OUT_BUFFER 5000

void init(char *user, char *database, char *response, char *buffer, char *input, int *valread, int sock)
{
    strcpy(database, "");
    strcpy(response, "");
    *valread = read(sock, database, OUT_BUFFER);

    strcpy(buffer, user);
    strcat(buffer, " ");
    strcat(buffer, database);
    strcat(buffer, " ");
}

int main(int argc, char const *argv[])
{
    // Initiate Client
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // IP Docker : 172.28.169.226
    // IP Local : 127.0.0.1
    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    // Important Variables
    char user[STR_BUFFER];
    char database[OUT_BUFFER], response[OUT_BUFFER];
    char buffer[CMD_BUFFER], input[CMD_BUFFER];

    // User
    if(getuid() == 0)
        strcpy(user, "root");
    else
        strcpy(user, "user");

    // Console Input
    if(isatty(fileno(stdin)))
    {
        int login = 1;

        while(1){
            init(user, database, response, buffer, input, &valread, sock);

            if(login){
                strcpy(input, "LOGIN");
                for(int i = 1; i < argc; ++i){
                    strcat(input, " ");
                    strcat(input, argv[i]);
                }
            }
            else {
                printf("%s=#", strcmp(database, "databases") == 0 ? "E07sql" : database);
                fgets(input, CMD_BUFFER, stdin);
                input[strcspn(input, "\n")] = 0;
            }
            strcat(buffer, input);

            send(sock, buffer, CMD_BUFFER, 0);

            if(strcasecmp(input, "EXIT") == 0) {
                exit(EXIT_SUCCESS);
            }

            printf("Waiting for response...\n");
            valread = read(sock, response, OUT_BUFFER);
            printf("%s\n", response);

            if(login){
                if(strcmp(response, "Login Success!") == 0){
                    if(strcmp(user, "user") == 0)
                        strcpy(user, argv[2]);
                    login = 0;
                }
                else {
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
    // File Input
    else {
        // Login
        init(user, database, response, buffer, input, &valread, sock);

        // checking
        strcpy(input, "READ");
        for(int i = 1; i < argc; ++i){
            strcat(input, " ");
            strcat(input, argv[i]);
        }
        strcat(buffer, input);
        send(sock, buffer, CMD_BUFFER, 0);

        printf("Waiting for response...\n");
        valread = read(sock, response, OUT_BUFFER);
        printf("%s\n", response);

        if(strcmp(response, "All checking success!") == 0){
            if(strcmp(user, "user") == 0)
                strcpy(user, argv[2]);
        }
        else {
            exit(EXIT_FAILURE);
        }

        // Read Input
        while(fgets(input, CMD_BUFFER, stdin) != NULL)
        {
            if(strcmp(input, "") != 0 && strcmp(input, "\n") != 0){
                // Preparation
                init(user, database, response, buffer, input, &valread, sock);

                // Input
                input[strcspn(input, "\n")] = 0;
                strcat(buffer, input);
                send(sock, buffer, CMD_BUFFER, 0);

                // Response
                printf("Waiting for response...\n");
                valread = read(sock, response, OUT_BUFFER);
                printf("%s\n", response);
            }
        }
        init(user, database, response, buffer, input, &valread, sock);
        strcat(buffer, "EXIT");
        send(sock, buffer, CMD_BUFFER, 0);
    }
}
